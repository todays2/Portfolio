﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
public class PlayerManager : Photon.MonoBehaviour
{
    public static GameObject LocalPlayerIntance;
    public AudioSource useSkillSound;

    RaycastHit hit;

    //SpeedUp Skill
    FirstPersonController speed;
    bool isFastSkillDuration; //지속시간 중인지
    bool isFastSkillCoolTime; //스킬 쿨타임 중인지
    public float fastSkillCoolTime = 20f; //스킬쿨타임
    [HideInInspector] public float fastSkillCurTime; //현재 쿨타임
    float fastSkillDuration = 3f; //지속시간
    float fastSkillDurationCurTime;
    float firstWalkSpeed;
    float firstRunSpeed;
    float rayY;
    //Blind Skill    
    bool blindCoolTime;
    public float blindSkillCoolTime = 30f;
    [HideInInspector] public float blindSkillCurTime;

    private void Awake()
    {
        if (photonView.isMine)
        {
            PlayerManager.LocalPlayerIntance = this.gameObject;

        }
        DontDestroyOnLoad(this.gameObject);
        speed = GetComponent<FirstPersonController>();


    }
    // Use this for initialization
    void Start()
    {
        fastSkillDurationCurTime = fastSkillDuration;
        fastSkillCurTime = fastSkillCoolTime;
        blindSkillCurTime = blindSkillCoolTime;
        firstWalkSpeed = speed.m_WalkSpeed;
        firstRunSpeed = speed.m_RunSpeed;
        //skillNum = Exit_DBAPI.instance.data.setSkill;
        if (photonView.isMine) //스킬 UI
        {
            GameObject.Find("Canvas").GetComponent<SkillUI>().SetTarget(this);
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (this.gameObject.CompareTag("Doll"))
        {
            if (Exit_GameManager.instance.escPopup.activeSelf ||
                Exit_GameResult.instance.panel_reward.activeSelf ||
                Exit_GameResult.instance.gameEnd ||
                GetComponent<PlayerHealth>().dead)
            {
                GetComponent<FirstPersonController>().enabled = false;
                GetComponent<CharacterController>().enabled = false;
                GetComponent<CatchCtrl>().enabled = false;
            }
            else if (photonView.isMine && !GetComponent<PlayerHealth>().stun && !GetComponent<HoldState>().iWasCaught)
            {
                GetComponent<FirstPersonController>().enabled = true;
                GetComponent<CharacterController>().enabled = true;
                GetComponent<CatchCtrl>().enabled = true;
            }
        }
        if (this.gameObject.CompareTag("Dead"))
        {
            if (Exit_GameManager.instance.escPopup.activeSelf ||
                Exit_GameResult.instance.panel_reward.activeSelf ||
                Exit_GameResult.instance.gameEnd)
            {
                GetComponent<FirstPersonController>().enabled = false;
                GetComponent<CharacterController>().enabled = false;
                GetComponent<DeathKnightAttack>().enabled = false;
                GetComponent<Blood_Deadman>().enabled = false;
                GetComponent<Dead_Scan>().enabled = false;
            }
            else if (photonView.isMine)
            {
                GetComponent<FirstPersonController>().enabled = true;
                GetComponent<CharacterController>().enabled = true;
                GetComponent<DeathKnightAttack>().enabled = true;
                GetComponent<Blood_Deadman>().enabled = true;
                GetComponent<Dead_Scan>().enabled = true;
            }
        }
        if (!Exit_GameManager.instance.loading.activeSelf)
        {
            if (this.GetComponent<PhotonView>().isMine && this.gameObject.CompareTag("Doll"))
            {
                switch (Exit_DBAPI.instance.data.setSkill)
                {
                    case 0:
                        {
                            if (Input.GetMouseButtonDown(1) && blindCoolTime == false) //Doll Blind Skill
                            {
                                //print("인형 블러드 사용");

                                if (Physics.Raycast(transform.position + new Vector3(0, transform.localScale.y * rayY, 0), transform.forward, out hit, 5f))
                                {       //디버그용 ↓
                                    Debug.DrawRay(transform.position + new Vector3(0, transform.localScale.y * rayY, 0), transform.forward * 5.0f, Color.red, 1.0f);
                                    //print("1단계");
                                    print("hit : " + hit.transform.name);
                                    if (hit.collider.gameObject.tag == "Map")
                                    {
                                        //print("IS Map");
                                        return;
                                    }
                                    else if (hit.collider.gameObject.tag == "Doll")
                                    {
                                        blindCoolTime = true;
                                        useSkillSound.clip = this.gameObject.GetComponent<PlayerAnimationController>().useBlindSound;
                                        useSkillSound.volume = 1f * Exit_DBAPI.instance.data.setSFX;
                                        useSkillSound.Play();
                                        //print("Is Doll");                                    
                                        int dollID = hit.collider.GetComponent<PhotonView>().viewID;
                                        //print("Doll ID :  " + dollID);
                                        photonView.RPC("FindChil", PhotonTargets.Others, 2, dollID);
                                    }
                                }
                            }
                            if (blindCoolTime)
                            {
                                blindSkillCurTime = blindSkillCurTime - Time.deltaTime;
                                if (blindSkillCurTime <= 0)
                                {
                                    //print("You can use Blind");
                                    blindSkillCurTime = blindSkillCoolTime;
                                    blindCoolTime = false;
                                }
                            }
                            break;
                        }
                    case 1:
                        {
                            //print(speed.m_WalkSpeed +"현재 스피드");
                            //print(firstWalkSpeed +"처음 스피드");
                            if (Input.GetMouseButton(1) && isFastSkillCoolTime == false) //Doll SpeedUP
                            {
                                //print("Skill 씀");
                                //speed.m_WalkSpeed = firstWalkSpeed + 1f;
                                speed.m_RunSpeed = firstRunSpeed + firstRunSpeed;
                                isFastSkillDuration = true;
                                isFastSkillCoolTime = true;
                                useSkillSound.clip = this.gameObject.GetComponent<PlayerAnimationController>().useFastSound;
                                useSkillSound.volume = 1f * Exit_DBAPI.instance.data.setSFX;
                                useSkillSound.Play();
                            }
                            if (isFastSkillCoolTime)
                            {
                                //print("쿨타임중");
                                fastSkillCurTime = fastSkillCurTime - Time.deltaTime;

                                if (fastSkillCurTime <= 0)
                                {
                                    fastSkillCurTime = fastSkillCoolTime;
                                    isFastSkillCoolTime = false;
                                    //print("스킬사용가능");
                                }
                            }
                            if (isFastSkillDuration)
                            {
                                fastSkillDurationCurTime = fastSkillDurationCurTime - Time.deltaTime;
                                if (fastSkillDurationCurTime < 0 && isFastSkillDuration)
                                {
                                    //print("지속시간 종료");
                                    fastSkillDurationCurTime = fastSkillDuration;
                                    isFastSkillDuration = false;
                                    speed.m_WalkSpeed = firstWalkSpeed;
                                    speed.m_RunSpeed = firstRunSpeed;
                                }
                            }
                            break;
                        }
                }
            }
        }
    }
}
