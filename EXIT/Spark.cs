﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spark : MonoBehaviour {
    public Light[] pointLights;
    
    public AudioClip sparki;
    AudioSource audio;
    float min = 0f; //white
    float max = 3.33f;

    float emin = 3.6f;
    float emax = 6.9f;

    float min3 = 0f;
    float max3 = 1f;

    float min4 = 0f;
    float max4 = 1.9f;

    float startTime = 1f;
    float curtime;
    float coolTime = 3f;
    float curCoolTime;
    float rdSpark;

    float soundmin = 0.5f;
    float soundmax = 0.9f;
    
    bool spark;
    bool volumeReset;
    private void Awake()
    {
        audio = GetComponent<AudioSource>();
        curtime = startTime;       
        audio.clip = sparki;        
    }
    // Use this for initialization
    void Start () {
        audio.volume = Random.Range(soundmin, soundmax);
        audio.Play();        
	}
    IEnumerator SparkLight()
    {
        if (spark)
        {
            curtime = curtime - Time.fixedDeltaTime;
            rdSpark = Random.Range(min, max);
            pointLights[0].intensity = rdSpark;
            pointLights[1].intensity = rdSpark;
            for (int i = 0; i < pointLights.Length; i++)
            {
                if (i !=0 && i !=1)
                {
                    pointLights[i].enabled = true;
                    //print(i);
                }
            }
            yield return new WaitForSeconds(0.1f);
            for (int i = 0; i < pointLights.Length; i++)
            {
                if (i != 0 && i != 1)
                {
                    pointLights[i].enabled = false;
                }
            }
        }
        if (curtime < 0)
        {
            spark = false;
            pointLights[0].intensity = 6.74f;
            pointLights[1].intensity = 6.74f;
        }
        if (spark == false)
        {
            audio.volume = 0f;
            for (int i = 0; i < pointLights.Length; i++)
            {
                pointLights[i].enabled = true;
            }

            curCoolTime = curCoolTime - Time.fixedDeltaTime;
        }
        if (curCoolTime < 0)
        {
            curtime = startTime;
            curCoolTime = coolTime;
            audio.volume = Random.Range(soundmin, soundmax);
            spark = true;
        }
        yield return null;
    }
    private void FixedUpdate()
    {
        StartCoroutine(SparkLight());
    }

}
