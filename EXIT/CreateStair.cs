﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.MyCompany.MyGame;

public class CreateStair : Photon.MonoBehaviour
{
    //계단생성 
    public static CreateStair ins;
    public Transform[] instantiatePositions;
    public GameObject[] prefabs;
    public static int index = 0;
    string stairTag;
    bool createAlReady;
    bool destroyAlReady;
    public Stair[] _stairs;
    GameObject[] stairs;
    [HideInInspector]
    public bool creatOver;
    int destroyindex;
    public PhotonView pv;
    //PhotonView photonView;
    private void Awake()
    {
        pv = this.gameObject.GetComponent<PhotonView>();
    }
    // Use this for initialization
    void Start()
    {
        ins = this;
        //photonView = PhotonView.Get(Dissolve.instance);            
        stairs = new GameObject[_stairs.Length];
    }

    // Update is called once per frame
    void Update()
    {

        if (PhotonNetwork.isMasterClient && Exit_GameManager.instance.craneMove && !creatOver)
        {
            CheckStairState();
            if (stairTag != null)
            {
                print("TagNoNull");
                createAlReady = true;
            }
            //if (createAlReady && PhotonNetwork.room.playerCount<4)
            //{
            //        print("호스트 계단생성");
            //   photonView.RPC("CreateStair",PhotonTargets.Others,index);
            //}
            //if (Input.GetKeyDown(KeyCode.Space)) //생성조건 들어갈 부분.
            //{
            //        photonView.RPC("CreateStair", PhotonTargets.All, null);

            //}
            //if (Input.GetKeyDown(KeyCode.E)) //파괴조건 들어갈 부분.
            //{
            //    photonView.RPC("DestroyStair", PhotonTargets.All, null);       
            //}
        }
    }
    [PunRPC]
    void CreateStairs(int stairIndex) //계단이 생성되어있다면 다른사람들에게도 알려줘야함.
    {
        if (!creatOver)
        {
            stairTag = prefabs[index].tag;
            prefabs[stairIndex].GetComponent<StairDissolve>().dissolving = true;
            stairs[stairIndex] = Instantiate(prefabs[stairIndex], instantiatePositions[stairIndex].position, instantiatePositions[stairIndex].transform.rotation);
            stairs[stairIndex].GetComponent<MeshCollider>().enabled = false;
            stairs[stairIndex].transform.parent = instantiatePositions[stairIndex].transform;
            for (int i = 0; i < instantiatePositions.Length; i++)
            {
                Destroy(instantiatePositions[i].GetChild(0).gameObject);
            }
            creatOver = true;
        }
    }
    [PunRPC]
    void DestroyStair() //계단 파괴.
    {
        if (StairDissolve.instance == null)
        {
            return;
        }
        GameObject[] _stairs = GameObject.FindGameObjectsWithTag(stairTag);
        for (int stairIndex = 0; stairIndex < _stairs.Length; stairIndex++)
        {
            _stairs[stairIndex].GetComponent<StairDissolve>().undissolving = true;
        }
        //stairs[destroyindex].GetComponentInChildren<CraneDissolve>().undissolving = true;
    }

    void CheckStairState()
    {
        for (int i = 0; i < _stairs.Length; i++)
        {
            if (_stairs[i].npcCount <= 0 && !_stairs[i].dissolving) // stairIndex인덱스의 카운트가 0이면 계단생성 효과.
            {
                photonView.RPC("CreateStairs", PhotonTargets.All, i);
                destroyindex = i;
            }
        }
    }
    //[PunRPC]
    public void CountCheck(int whatbasket)
    {
        for (int i = 0; i < _stairs.Length; i++)
        {
            if (_stairs[i].npcCount != 1)
            {
                _stairs[i].npcCount--;
            }
            else if (_stairs[i].npcCount <= 1)
            {
                _stairs[whatbasket].npcCount--;
            }
        }
    }
}
[System.Serializable]
public class Stair
{
    public int npcCount;
    public bool dissolving;
    public bool undissolving;
}


