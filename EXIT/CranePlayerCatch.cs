﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CranePlayerCatch : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {   
        if(!CraneCtrl.ins.catchNow)
        {
            if (other.CompareTag("Doll"))
            {
                CraneCtrl.ins.pickPlayer = other.gameObject;
                print("플레이어 들어옴 : " +CraneCtrl.ins.pickPlayer.gameObject.name);
                CraneCtrl.ins.catchNow = true; //플레이어를 잡아라            
            }
        }               
    }
    private void OnTriggerExit(Collider other)
    {       
        if (other.CompareTag("Doll"))
        {
            CraneCtrl.ins.pickPlayer = null;
            print("플레이어 벗어남");
            CraneCtrl.ins.catchNow = false; //플레이어를 잡아라
            CraneCtrl.ins.craneCatchLeftTime = CraneCtrl.ins.craneCatchStartTime; //타이머 초기화
            //CraneCtrl.ins.craneDonCatchLeftTime = CraneCtrl.ins.craneDonCatchStartTime;
        }
    }
}

