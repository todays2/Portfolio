﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class DollRotation : MonoBehaviour
{
    public Camera cam;
    public Transform look;
    public bool roll;
    Quaternion ch;
    Animator ani;

    [Header("Dont")]
    public GameObject[] pan;
    

    // Update is called once per frame
    void Update()
    {
        if (!pan[0].activeSelf && !pan[1].activeSelf && !pan[2].activeSelf && !pan[3].activeSelf)
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RayCasting(ray);
        }
    }
    void RayCasting(Ray ray)
    {
        float hor = CrossPlatformInputManager.GetAxis("Mouse X") * 10f;
        //float hor = Input.GetAxis("Mouse X") * 10f;
        //print(hor);
        //print(roll);

        RaycastHit hitob;
        if (Physics.Raycast(ray, out hitob, Mathf.Infinity))
        {
            if (hitob.transform.CompareTag("Doll") && Input.GetMouseButton(0))
            {
                roll = true;
            }
            if (Input.GetMouseButtonDown(1))
            {
                ani = GetComponentInChildren<Animator>();
                bool startMotions = ani.GetBool("StartMotions");
                startMotions = !startMotions;
                ani.SetBool("StartMotions", startMotions);
            }
        }
        if (roll)
        {
            if (Input.GetMouseButton(0))
            {
                print("돌리는중");
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;

                ch *= Quaternion.Euler(0f, hor, 0f);
                //transform.rotation *= Quaternion.AxisAngle(Vector3.up, hor * Time.deltaTime);
                transform.localRotation *= Quaternion.Euler(new Vector3(0, -hor, 0));
            }
            else if (Input.GetMouseButtonUp(0))
            {
                roll = false;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;

            }

        }
        else
        {
            Vector3 vr = cam.transform.position - transform.position;
            Quaternion a = Quaternion.LookRotation(vr);
            transform.rotation = Quaternion.Slerp(transform.rotation, look.transform.rotation, 0.05f);
        }
    }

}
