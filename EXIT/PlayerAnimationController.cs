﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;


public class PlayerAnimationController : MonoBehaviour
{
    FirstPersonController fpc;
    Animator anim;
    CharacterController controller;

    PhotonView pv;
    GameObject mysoundOb;
    AudioSource sfx_myAudio;
    public AudioClip sfx_myWalkSound;
    public AudioClip sfx_myRunSound;
    public AudioClip sfx_myStunSound;
    public AudioClip useBlindSound;
    public AudioClip useFastSound;
    bool isWalk;
    bool isRun;
    bool isJump;
    bool isStun;
    bool isDead;

    void Start()
    {
        fpc = GetComponent<FirstPersonController>();
        anim = GetComponentInChildren<Animator>();
        controller = GetComponent<CharacterController>();
        sfx_myAudio = GetComponent<AudioSource>();
        pv = GetComponent<PhotonView>();
    }
    // Update is called once per frame
    void Update()
    {
        if (!GetComponent<PlayerHealth>().dead && !this.gameObject.GetComponent<MyeffectCheck>().effect)
        {
            if (GetComponent<PlayerHealth>().stun || GetComponent<HoldState>().iWasCaught)
            {
                if (!isStun)
                {
                    pv.RPC("DollSoundState", PhotonTargets.All, pv.viewID, 5);
                }
            }
            else if (GetComponent<PlayerHealth>().stun == false)
            {
                isStun = false;
            }
            if (controller.velocity.magnitude < 0.1f)
            {
                anim.SetBool("IsWalk", false);
            }
            else if (controller.isGrounded)
            {
                anim.SetBool("IsWalk", true);
            }
            anim.SetBool("IsRun", fpc.m_IsRun);
            /////////////////////////////////////////////
            if (controller.velocity.magnitude < 0.1f)
            {
                pv.RPC("DollSoundState", PhotonTargets.All, pv.viewID, 1);
                isWalk = false;
            }
            else if (!isWalk && !isRun && controller.isGrounded)
            {
                pv.RPC("DollSoundState", PhotonTargets.All, pv.viewID, 2);
                isWalk = true;
            }
            if (fpc.m_IsRun && !isRun)
            {
                pv.RPC("DollSoundState", PhotonTargets.All, pv.viewID, 3);
                isRun = true;
                isWalk = false;
            }
            else if (!fpc.m_IsRun)
            {
                pv.RPC("DollSoundState", PhotonTargets.All, pv.viewID, 4);
                isRun = false;
            }
            if (fpc.m_Jumping)
            {
                anim.SetBool("IsJump", true);
                isWalk = false;
                isRun = false;
            }
            else
            {
                anim.SetBool("IsJump", false);
            }
        }
        else if (GetComponent<PlayerHealth>().dead)
        {
            isWalk = false;
            isRun = false;
            isStun = false;
            anim.SetBool("IsDied", true);
        }
        if (GetComponent<MyeffectCheck>().effect)
        {
            sfx_myAudio.volume = 1f * Exit_DBAPI.instance.data.setSFX;
            sfx_myAudio.loop = false;
        }
    }
    [PunRPC]
    void DollSoundState(int id, int state)
    {
        mysoundOb = PhotonView.Find(id).gameObject;
        mysoundOb.GetComponent<AudioSource>().loop = true;
        switch (state)
        {
            case 1: //Idle
                {
                    mysoundOb.GetComponent<AudioSource>().volume = 0f;
                    mysoundOb.GetComponent<AudioSource>().pitch = 1f;

                    break;
                }
            case 2: //Walk
                {
                    mysoundOb.GetComponent<AudioSource>().clip = sfx_myWalkSound;
                    mysoundOb.GetComponent<AudioSource>().volume = 0.2f * Exit_DBAPI.instance.data.setSFX;
                    mysoundOb.GetComponent<AudioSource>().pitch = 0.6f;
                    mysoundOb.GetComponent<AudioSource>().Play();
                    break;
                }
            case 3://Run
                {
                    mysoundOb.GetComponent<AudioSource>().clip = sfx_myRunSound;
                    mysoundOb.GetComponent<AudioSource>().volume = 0.5f * Exit_DBAPI.instance.data.setSFX;
                    mysoundOb.GetComponent<AudioSource>().pitch = 0.75f;
                    mysoundOb.GetComponent<AudioSource>().Play();
                    break;
                }
            case 4://No Run
                {
                    mysoundOb.GetComponent<PlayerAnimationController>().isRun = false;
                    break;
                }
            case 5:
                {
                    mysoundOb.GetComponent<PlayerAnimationController>().isStun = true;
                    mysoundOb.GetComponent<AudioSource>().clip = sfx_myStunSound;
                    mysoundOb.GetComponent<AudioSource>().volume = 0.5f * Exit_DBAPI.instance.data.setSFX;
                    mysoundOb.GetComponent<AudioSource>().pitch = 1f;
                    mysoundOb.GetComponent<AudioSource>().Play();
                    break;
                }
        }

    }
}
