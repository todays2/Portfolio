﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class HoldState : MonoBehaviour
{

    public static HoldState ins;
    public bool holdNow; // 들고있다면 True (Player)
    public bool iWasCaught; // 내가 잡히면 True (NPC/Player)
    public bool craneHolding;
    FirstPersonController fp;
    private void Start()
    {
        fp = GetComponent<FirstPersonController>();
    }
    private void FixedUpdate()
    {
        if (this.CompareTag("NPC") == false)
        {
            if (iWasCaught)
            {
                fp.caught = true;
            }
            else
            {
                fp.caught = false;
            }
        }
    }
}
