﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class MapGenerator : MonoBehaviour {
    public static MapGenerator inst;
    public Transform tilePrefab;
    Vector2 mapSize;	
    public float tileSize;
    [Range(0,1)]
    public float outLienPercent;

    GameObject[] tiles;
    GameObject[] canMoveTiles;
    GameObject[] donMoveTiles;
    [HideInInspector]
    public List<GameObject> canMoveList = new List<GameObject>();
    List<GameObject> donMoveList = new List<GameObject>();
    GameObject[] ddf;
    string holderName = "Generated Map";

    bool chek = false;
    private void Awake()
    {
        inst = this;
        tiles = GameObject.FindGameObjectsWithTag("Tile");
        mapSize.x = TileMap_KS.mapSize.x;
        mapSize.y = TileMap_KS.mapSize.y;
        canMoveList.Clear();
        donMoveList.Clear();
        Generator();
        canMoveTiles = new GameObject[tiles.Length];
        donMoveTiles = new GameObject[canMoveTiles.Length];
        TileCheck();       
        
    }
    private void Start()
    {
        //for (int i = 0; i < donMoveList.Count; i++)
        //{
        //    donMoveList[i].gameObject.GetComponent<Renderer>().material.color = Color.red;
        //}
        //for (int i = 0; i < canMoveList.Count; i++)
        //{
        //    canMoveList[i].gameObject.GetComponent<Renderer>().material.color = Color.green;
        //}
    }


    public void Generator()
    {
        tilePrefab.transform.localScale = new Vector3(tileSize, tileSize, tileSize);

        if (transform.Find(holderName))
        {            
            return;
        }
        
        else
        {
            Create();
        }               
    }
    public void Create()
    {
        Transform mapHolder = new GameObject(holderName).transform;
        mapHolder.parent = transform;
        for (int x = 0; x < mapSize.x; x++)
        {
            for (int y = 0; y < mapSize.y; y++)
            {
                //Vector3 tilePosition = new Vector3((-mapSize.x)*2 + (tileSize/2) + x*10 - 30, 2, (-mapSize.y) * 2 + (tileSize/2) + y*10-30);
                Vector3 tilePosition = new Vector3(((-mapSize.x + 1) / 2) * (tileSize) + x * tileSize, 0.1f, ((-mapSize.y + 1) / 2) * (tileSize) + y * tileSize);
                Transform newTile = Instantiate(tilePrefab, tilePosition, Quaternion.Euler(Vector3.right * 90));
                newTile.localScale = Vector3.one * (tileSize - outLienPercent);
                newTile.parent = mapHolder;
            }
        }
    }
    void TileCheck()
    {        
        for(int i = 0,j=0,k=0; i<tiles.Length; i++)
        {
            if (tiles[i].GetComponent<CanNotMove>().canMove==false)
            {
                donMoveList.Add(tiles[i].gameObject);
                k++;
                //donMoveList[k - 1].gameObject.GetComponent<Renderer>().material.color = Color.red;
            }
            else if (tiles[i].GetComponent<CanNotMove>().canMove==true)
            {
                canMoveList.Add(tiles[i].gameObject);
                j++;
                //canMoveList[j - 1].gameObject.GetComponent<Renderer>().material.color = Color.green;
            }
        }                                                     
    }        
}
