﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lighting : MonoBehaviour {
    public  Light point;
    public Light point2;
    public Light point3;
    public Light point4;
    public Light point5;
    float min = 0f; //white
    float max = 3.33f;

    float emin = 3.6f;
    float emax = 6.9f;

    float min3 = 0f;
    float max3 = 1f;

    float min4 = 0f;
    float max4 = 1.9f;

    float startTime = 1f;
    float curtime;
    float coolTime = 3f;
    float curCoolTime;
    float dd;

    bool spark = true;
	// Use this for initialization
	void Start () {
        curtime = startTime;       
	}
    private void FixedUpdate()
    {
        if (spark)
        {            
            curtime = curtime - Time.deltaTime;
            dd = Random.Range(min, max);
            point.intensity = dd;
            point2.intensity = dd;
            if(point3.enabled == true)
            {
                point3.enabled = false;
                point4.enabled = false;
                point5.enabled = false;
            }
            else
            {
                point3.enabled = true;
                point4.enabled = true;
                point5.enabled = true;
            }
        }
        if (curtime < 0)
        {
            spark = false;
            point.intensity = 6.74f;
        }
        if(spark == false)
        {
            point3.enabled = true;
            point4.enabled = true;
            point5.enabled = true;
            curCoolTime = curCoolTime - Time.deltaTime;
        }
        if (curCoolTime < 0)
        {
            curtime = startTime;
            curCoolTime = coolTime;
            spark = true;
        }        
    } 
}
