﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dead_Scan : MonoBehaviour {

    public GameObject minimap;
    GameObject map;
    bool useSkill_Scan;
    public AudioClip useScanSound;
    [HideInInspector] public float coolTime_Scan = 10f;
    [HideInInspector] public float leftTime_Scan;

    PhotonView pv;

    private void Awake()
    {
        pv = this.GetComponent<PhotonView>();
                
        leftTime_Scan = coolTime_Scan;
    }

    private void Start()
    {
        if (pv.isMine)
        {
            GameObject minimap = Exit_GameManager.instance.minimap;
            //minimap.SetActive(false);
            map = minimap.transform.GetChild(0).gameObject;
            map.SetActive(false);

            GameObject.Find("Slider_Energy").GetComponent<PlayerHealthUI>().SetTarget(this);
        }
    }

    private void FixedUpdate()
    {
        if (pv.isMine)
        {
            if (!Exit_GameResult.instance.panel_reward.activeSelf && !Exit_GameManager.instance.escPopup.activeSelf)
            {
                if (Input.GetKeyDown(KeyCode.Q) && !useSkill_Scan)
                {
                    useSkill_Scan = true;
                    leftTime_Scan = coolTime_Scan;
                    this.gameObject.GetComponent<PlayerManager>().useSkillSound.clip = useScanSound;
                    this.gameObject.GetComponent<PlayerManager>().useSkillSound.volume = 1f * Exit_DBAPI.instance.data.setSFX;
                    this.gameObject.GetComponent<PlayerManager>().useSkillSound.Play();
                    StartCoroutine(Scan());
                }
                if(useSkill_Scan)
                {
                    leftTime_Scan = leftTime_Scan - Time.fixedDeltaTime;
                    if(leftTime_Scan < 0)
                    {
                        useSkill_Scan = false;
                        leftTime_Scan = coolTime_Scan;
                    }
                }                
            }
        }
    }

    IEnumerator Scan()
    {
        map.SetActive(true);
        yield return new WaitForSeconds(5f);

        map.SetActive(false);
    }
}
