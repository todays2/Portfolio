﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.MyCompany.MyGame
{
    public class Basket : MonoBehaviour {
        public int basketIndex;
                        
        PhotonView pvS;
        
        private void Start()
        {
            pvS = GetComponentInParent<PhotonView>(); //부모가 가진 photonview를 받아온다.
        }
        private void Update()
        {            
            if (CreateStair.ins.creatOver) //계단생성이 완료 됬다면 해당 script를 Destroy한다.
            {
                Destroy(this);
            }
        }
        //private void OnTriggerStay(Collider other)
        //{
        //    if (other.gameObject.CompareTag("NPC") && other.GetComponent<Rigidbody>().useGravity)
        //    {
        //        CreateStair.ins._stairs[basketIndex].npcCount--;
        //        Destroy(other.gameObject);
        //    }
        //    if(CreateStair.ins._stairs[basketIndex].npcCount == 0 && !basketOff)
        //    {
        //        for (int i = 0; i < baskets.Length; i++)
        //        {
        //            Destroy(baskets[i]);

        //        }
        //        basketOff = true;
        //    }
        //}
        private void OnTriggerEnter(Collider other) //닿은 오브젝트가 NPC라면 Count를 -- 한다.
        {
            if (Exit_GameManager.instance.craneMove)
            {
                if (other.gameObject.CompareTag("NPC") && other.GetComponent<Rigidbody>().useGravity)
                {
                        Destroy(other.gameObject); //닿은 오브젝트를 없앤다.
                    //if (pvS.isMine)
                    //{
                    //    pvS.RPC("CountCheck", PhotonTargets.All, basketIndex); //네트워크 동기화(Count)
                    //}
                    CreateStair.ins.CountCheck(basketIndex);

                }            
            }
        }            
    }

}
