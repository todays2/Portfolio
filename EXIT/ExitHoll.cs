﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ExitHoll : MonoBehaviour
{
    public bool isglass;
    public GameObject glass;
    public Collider reapercollision;
    bool exit;
    public bool isWall;
    private void OnTriggerEnter(Collider other)
    {
        if (Exit_GameManager.instance.craneMove && !isWall)
        {
            if (other.CompareTag("Doll")) //게임 시작일 경우에만
            {
                if (isglass) //유리인지 아닌지 (유리의 탈출과 탈출구의 탈출을 같은 Script로 사용하기 위함)
                {
                    if (GetComponentInParent<GlassHp>().broken && exit == false)
                    {
                        glass.GetComponent<BoxCollider>().enabled = true;

                        //other.GetComponent<PhotonView>().RPC("DollExit", PhotonTargets.All, other.GetComponent<PhotonView>().viewID);
                        if (other.GetComponent<PhotonView>().isMine)
                        {
                            other.GetComponent<PhotonView>().RPC("UserinfoIconChange_byExit", PhotonTargets.All, other.GetComponent<NickName>().nickNameLabel.text);
                            Exit_GameResult.instance.exit = true;
                            Exit_GameResult.instance.SomeoneExit();
                            Exit_GameResult.instance.GetReward();
                        }
                        GetComponentInParent<GlassHp>().broken = false;
                        exit = true;
                        print("Glass Exit");
                    }
                }
                else
                {
                    if (other.GetComponent<PhotonView>().isMine)
                    {
                        //other.GetComponent<PhotonView>().RPC("DollExit", PhotonTargets.All, other.GetComponent<PhotonView>().viewID);
                        if (other.gameObject.GetComponent<HoldState>().craneHolding == false)
                        {
                            GameObject.Find("Stairs").GetComponent<PhotonView>().RPC("DestroyStair", PhotonTargets.All, null);
                        }

                        other.GetComponent<PhotonView>().RPC("UserinfoIconChange_byExit", PhotonTargets.All, other.GetComponent<NickName>().nickNameLabel.text);
                        Exit_GameResult.instance.exit = true;
                        Exit_GameResult.instance.SomeoneExit();
                        Exit_GameResult.instance.GetReward();
                        print("Exit");
                    }
                    //other.GetComponent<PhotonView>().RPC("DollExit", PhotonTargets.All, other.GetComponent<PhotonView>().viewID);
                }
                print("Doll in the Exit Box");
            }
        }        
        else if(isWall && other.CompareTag("Doll") && !Exit_GameManager.instance.craneMove)
        {            
                print("GOHome");
            for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
            {
                GameObject go = Exit_GameManager.instance.
                photonPlayerList[i].TagObject as GameObject;
                if (go == other.gameObject)
                {
                    go.transform.position = Exit_GameManager.instance.spawnPosition.position;
                    go.GetComponent<PlayerHealth>().health = 100;
                }
            }
        }
    }
}
