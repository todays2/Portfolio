﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCKinematic : MonoBehaviour {
    Rigidbody npc;
	// Use this for initialization
	void Start () {
        npc = GetComponent<Rigidbody>();
        npc.isKinematic = true;
	}
    private void Update()
    {
        if (npc.isKinematic && Exit_GameManager.instance.craneMove)
        {
            npc.isKinematic = false;
            Destroy(this);
        }
    }

}
