﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class MyeffectCheck : Photon.MonoBehaviour
{
    //[HideInInspector]
    public bool effect;
    public MyeffectCheck ins;
    // Update is called once per frame
    GameObject[] dolls;
    GameObject myDoll;
    public GameObject bloodPrefab;
    GameObject bl;
    PhotonView pv;
    float skillTime = 8f;
    //[HideInInspector]
    public bool gobloody;
    public AudioSource blindSource;
    public AudioClip blindSound;
    public AudioClip blindSound2;
    int _who;
    private void Start()
    {
        pv = GetComponent<PhotonView>();
        ins = this;
        myDoll = this.gameObject;       
    }
    void Update()
    {
        if (this.gameObject.CompareTag("Doll") && pv.isMine)
        {
            //print("난 플레이어다");
            if (effect && gobloody)
            {
                this.transform.GetChild(1).GetComponent<MotionBlur>().enabled = true;
            }
            else
            {
                this.transform.GetChild(1).GetComponent<MotionBlur>().enabled = false;
            }
        }
        if (myDoll != null)
        {
            if (myDoll.GetComponent<MyeffectCheck>().effect && gobloody == false)
            {
                gobloody = true;
                StartCoroutine(GoEffect(_who));
            }
        }
    }
    [PunRPC]
    void FindChil(int ch , int id) // 1 = DeathNight , 2 = Doll
    {
        print("받았다");
        switch (ch)
        {
            case 1:
                {
                    dolls = GameObject.FindGameObjectsWithTag("Doll");
                    print("Count :" + dolls.Length);
                    int[] ids = new int[dolls.Length];
                    for (int i = 0; i < ids.Length; i++)
                    {
                        ids[i] = dolls[i].GetComponent<PhotonView>().viewID;
                        if (PhotonView.Find(ids[i]).isMine)
                        {
                            myDoll = PhotonView.Find(ids[i]).gameObject;
                            print("블라인드");
                        }
                    }
                    myDoll.GetComponent<MyeffectCheck>().effect = true;
                    myDoll.GetComponent<MyeffectCheck>()._who = 1;
                    break;
                }
            case 2:
                {
                    myDoll = PhotonView.Find(id).gameObject;
                    if (myDoll.GetComponent<PhotonView>().isMine)
                    {
                        myDoll.GetComponent<MyeffectCheck>().effect = true;
                        myDoll.GetComponent<MyeffectCheck>()._who = 2;
                    }
                    
                    break;
                }
        }
    }

    IEnumerator GoEffect(int who)
    {
        switch (who) //1 = Reaper , 2 = Doll
        {
            case 1:
                {
                    bl = Instantiate(bloodPrefab, GameObject.Find("Canvas").transform);
                    bl.SetActive(true);
                    bl.GetComponent<Animator>().SetBool("blood", true);
                    blindSource.clip = blindSound;
                    blindSource.volume = 1f;
                    blindSource.Play();
                    yield return new WaitForSeconds(skillTime);
                    bl.SetActive(false);
                    myDoll.GetComponent<MyeffectCheck>().effect = false;
                    gobloody = false;
                    Destroy(bl);
                    blindSource.volume = 0f;
                    break;
                }
            case 2:
                {
                    bl = Instantiate(bloodPrefab, GameObject.Find("Canvas").transform);
                    bl.SetActive(true);
                    bl.GetComponent<Animator>().SetBool("blood", true);
                    blindSource.clip = blindSound2;
                    blindSource.volume = 1f;
                    blindSource.Play();
                    yield return new WaitForSeconds(skillTime);
                    bl.SetActive(false);
                    myDoll.GetComponent<MyeffectCheck>().effect = false;
                    gobloody = false;
                    Destroy(bl);
                    blindSource.volume = 0f;
                    break;
                }
        }
        
        print("이펙트 종료");
    }
}
