﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.MyCompany.MyGame;


    public class StairDissolve : Photon.MonoBehaviour{
    public GameObject dissolveGameObject;
    public Material[] dissolveMaterials;
    public static StairDissolve instance;
    [HideInInspector] public bool dissolving;
    [HideInInspector] public bool undissolving;
    public float speed;
    float max = 1f;
    float min;        
    float startTime, currentDissolve;
   
	// Use this for initialization
	void Start () {
        instance = this;
        for (int i = 0; i < dissolveMaterials.Length; i++)
        {
            dissolveMaterials[i].SetFloat("_Progress", 0);
        }              
	}	
	// Update is called once per frame	
        private void FixedUpdate()
        {            
            if (dissolving)
            {
                Dissolving();
                undissolving = false;
            }
            if (undissolving)
            {
                UnDissolving();
                dissolving = false;
            }           
        }
        public void DissolveSetMax()
        {
            for (int i = 0; i < dissolveMaterials.Length; i++)
            {
                dissolveMaterials[i].SetFloat("_Progress", 1);
            }
                this.gameObject.GetComponent<MeshCollider>().enabled = true;
            dissolving = false;
        }
    public void DissolveSetMin()
    {
        
        for (int i = 0; i < dissolveMaterials.Length; i++)
        {
            dissolveMaterials[i].SetFloat("_Progress", 0);
        }
        CreateStair.index = 0;
        Destroy(this.gameObject);
    }
    public void Dissolving()
    {
		if(currentDissolve< max)
        {
            for (int i = 0; i < dissolveMaterials.Length; i++)
            {
                dissolveMaterials[i].SetFloat("_Progress", currentDissolve);
            }
            currentDissolve+= speed * Time.fixedDeltaTime;               
        }
        else
        {
            DissolveSetMax();
        }
    }
        private void UnDissolving()
        {
            this.gameObject.GetComponent<MeshCollider>().enabled = false;
            if (currentDissolve > min)
            {
                for (int i = 0; i < dissolveMaterials.Length; i++)
                {
                    dissolveMaterials[i].SetFloat("_Progress", currentDissolve);
                }
                currentDissolve -= speed * Time.fixedDeltaTime;
            }
            else
            {
                //this.gameObject.GetComponent<MeshCollider>().enabled = false;
                DissolveSetMin();
            }
        
        }    
    }

