﻿using System.Collections;


public interface IFadable
{
    IEnumerator FadeIn();
    IEnumerator FadeOut();
}

//public IEnumerator FadeIn()
//{
//    isfadeIn = true;
//    isfadeOut = false;
//    //print("FadeIn");
//    fadeImage.gameObject.SetActive(true);
//    Color color = fadeImage.color;
//    float lerp = 0f;
//    while (fadeImage.color.a < 1 && !isfadeOut)
//    {
//        lerp += Time.deltaTime / fadeTime;
//        color.a = Mathf.Lerp(fadeEnd, fadeStart, lerp);
//        fadeImage.color = color;
//        yield return null;
//    }
//}

//public IEnumerator FadeOut()
//{
//    isfadeIn = false;
//    isfadeOut = true;
//    //print("FadeOut");
//    //print(fadeImage.color.a);
//    Color color = fadeImage.color;
//    float lerp = 0f;
//    while (fadeImage.color.a > 0 && !isfadeIn)
//    {
//        lerp += Time.deltaTime / fadeTime;
//        color.a = Mathf.Lerp(fadeStart, fadeEnd, lerp);
//        fadeImage.color = color;
//        yield return null;
//    }
//    if (!isfadeIn)
//    {
//        fadeImage.gameObject.SetActive(false);
//    }
//}
