﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

[RequireComponent(typeof(PhotonView))]
public class CraneCtrl : Photon.MonoBehaviour
{

    Transform[] movePoint;
    Transform currentpoint;
    public Transform craneHomePos;
    public Transform craneDownPos;
    public Transform craneUpPos;
    public GameObject crane; //잡는부분 내려가는 위치 Y정보만 사용. (x-z는 this.transform.position)
    public Transform pickPoint;
    [HideInInspector]
    public Transform cranePosition;
    public static CraneCtrl ins;

    [HideInInspector]
    public GameObject pickPlayer;

    float positionX;
    float positionY;

    float moveStartTime = 60f;                  //게임 시작 후 행동 카운트
    float moveLeftTime;
    [HideInInspector]
    public float craneDonCatchStartTime = 5f; //안잡혔을때 카운트
    [HideInInspector]
    public float craneDonCatchLeftTime;
    [HideInInspector]
    public float craneCatchStartTime = 5f; //잡혔을때 카운트
    [HideInInspector]
    public float craneCatchLeftTime;
    float craneCatchSync = 3f;
    float curCraneCatchSync;
    float craneOffCount;



    int positionpoint;


    bool catchCount;                //크레인 잡은 카운트중
    bool donCatchCount;             //크레인 못잡은 카운트
    bool craneState;                //크레인 행동끝났는지
    bool craneMoving;               //크레인 이동중인지
    bool craneDown;                 //크레인 내려가는중인지
    [HideInInspector]
    public bool catchPlayer;       //플레이어 잡으려는지
    [HideInInspector]
    public bool catchNow;           //플레이어를 잡고있는지    
    bool craneCatchAndUp;           //크레인 잡고 올라가려는지
    bool craneHome;                 //크레인 홈위치로 돌아가려는지
    bool craneThrow;                //크레인 플레이어 던지는지


    GameObject holdPlayer;

    public AudioSource sfx_craneAudio; //크레인 On/Off Source 사운드
    public AudioClip sfx_CraneOff;
    public AudioClip sfx_CraneOn;
    bool craneOn = true;
    bool craneOff = false;

    public AudioSource sfx_CraneState; //현재 크레인의 상태 사운드
    public AudioClip sfx_craneMove;
    public AudioClip sfx_CraneDown;
    public AudioClip sfx_CraneFail;
    public AudioClip sfx_CraneSuccess;

    bool isCraneMove; //크레인이 움직이나
    bool isCraneDown; //내려가나
    bool isCraneFail; //인형 잡는걸 실패했나
    bool isCraneSuccess; //인형 잡는걸 성공했나
    // wait , grab , throwing
    Animator craneanim;

    // Update is called once per frame
    private void Start()
    {
        ins = this;
        //카운트 설정
        moveLeftTime = moveStartTime;
        craneDonCatchLeftTime = craneDonCatchStartTime;
        craneCatchLeftTime = craneCatchStartTime;
        curCraneCatchSync = craneCatchSync;
        craneOffCount = craneCatchSync;

        //Bool값 초기화

        catchCount = false;
        donCatchCount = false;
        craneState = false;
        craneMoving = false;
        craneDown = false;
        catchPlayer = false;
        craneCatchAndUp = false;
        craneHome = false;
        catchNow = false;
        craneThrow = false;

        //움직일 수 있는 공간정보
        movePoint = new Transform[MapGenerator.inst.canMoveList.ToArray().Length];
        MovePointCheck();
        currentpoint = this.transform;

        // print("Crane Position: :x" + currentpoint.transform.position.x);
        //print("Crane Position: :Z" + currentpoint.transform.position.z);
        //sfx_craneAudio = GetComponent<AudioSource>();
        craneanim = GetComponentInChildren<Animator>();
    }
    private void FixedUpdate()
    {           //DB ON                                 DB Off

        if (Exit_GameManager.instance.craneMove)
        {
            if (craneOn)
            {
                sfx_CraneState.clip = sfx_CraneOn;
                sfx_CraneState.Play();
                craneOn = false;
            }
            if (craneOff)
            {
                craneOffCount = craneOffCount - Time.deltaTime;
                if (craneOffCount < 0)
                {
                    sfx_CraneState.clip = sfx_CraneOff;
                    sfx_CraneState.Play();
                    craneOff = false;
                }
            }
            CraneState();
        }

    }
    void MovePointCheck()
    {
        for (int i = 0; i < MapGenerator.inst.canMoveList.ToArray().Length; i++)
        {
            movePoint[i] = MapGenerator.inst.canMoveList[i].gameObject.transform;
        }
    }
    //IEnumerator GoRandomPosition() // Not OnLine
    // {        
    //     cranePosition = movePoint[Random.Range(0, movePoint.Length)].transform;        
    //     moveLeftTime = moveStartTime;
    //     craneMoving = true;
    //     yield return craneMoving;
    // }
    [PunRPC] //OnLine
    void SendGoPosition(int tr)
    {
        cranePosition = movePoint[tr].transform;
        craneMoving = true;
        craneState = true;
        //moveLeftTime = moveStartTime;
    }
    IEnumerator CraneMove()
    {
        if (!isCraneMove)
        {
            sfx_craneAudio.clip = sfx_craneMove;
            sfx_craneAudio.loop = true;
            sfx_craneAudio.Play();
            isCraneMove = true;
            craneanim.SetBool("throwing", false);
        }
        Vector3 ver = cranePosition.transform.position;
        Vector3 cur = transform.position;
        if (Vector3.Distance(new Vector3(ver.x, 0, ver.z), new Vector3(transform.position.x, 0, transform.position.z)) > 0.2f)
        {
            transform.position = Vector3.Lerp(new Vector3(cur.x, cur.y, cur.z), new Vector3(ver.x, cur.y, ver.z), 1 * Time.fixedDeltaTime);
            //print("이동중");
        }
        //transform.Translate(cranePosition.transform.position.x, 0, cranePosition.transform.position.z);
        else if (Vector3.Distance(new Vector3(ver.x, 0, ver.z), new Vector3(transform.position.x, 0, transform.position.z)) < 2f)
        {
            // print("이동완료");
            transform.position = new Vector3(ver.x, cur.y, ver.z);
            craneMoving = false;
            craneDown = true;
            yield return craneMoving;
        }
    }
    IEnumerator CraneDown()
    {
        if (!isCraneDown)
        {
            isCraneMove = false;
            sfx_craneAudio.clip = sfx_CraneDown;
            sfx_craneAudio.loop = false;
            sfx_craneAudio.Play();
            isCraneDown = true;
        }

        Vector3 curPos = transform.position; //현재 포지션.
        Vector3 downPos = craneDownPos.transform.position;//내려갈 포지션
        if (Vector3.Distance(new Vector3(curPos.x, crane.transform.position.y, curPos.z), new Vector3(curPos.x, downPos.y, curPos.z)) > 0.2f)
        {
            crane.transform.position = Vector3.Lerp(new Vector3(curPos.x, crane.transform.position.y, curPos.z), new Vector3(curPos.x, downPos.y, curPos.z), 1 * Time.fixedDeltaTime);
            //print("크레인 내려가는중");
        }
        else if (Vector3.Distance(new Vector3(curPos.x, crane.transform.position.y, curPos.z), new Vector3(curPos.x, downPos.y, curPos.z)) < 3f)
        {
            // print("다 내려옴");
            crane.transform.position = new Vector3(curPos.x, downPos.y, curPos.z);
            craneDown = false;
            catchPlayer = true;
            craneanim.SetBool("wait", true);
            yield return craneDown;
        }
    }
    IEnumerator CraneCatch()
    {
        if (pickPlayer != null)
        { //잡은게 있으면 5초후 잡은채로 올라감.
            if (craneCatchLeftTime > 0)
            {
                craneCatchLeftTime = craneCatchLeftTime - Time.fixedDeltaTime;
            }
            else
            {
                catchCount = true;
            }
        }
        if (craneCatchLeftTime < 0 && catchCount && !craneCatchAndUp)
        {
            print("잡고있다");

            photonView.RPC("PlayerCatch", PhotonTargets.All, pickPlayer.GetComponent<PhotonView>().viewID);
            curCraneCatchSync = curCraneCatchSync - Time.deltaTime;
            if (curCraneCatchSync < 0)
            {
                craneCatchAndUp = true;
            }
            yield return null;
        }
        else if (pickPlayer == null)
        {   //잡은게 없으면 일정시간후 그냥 올라감.
            if (craneDonCatchLeftTime > 0)
            {
                craneDonCatchLeftTime = craneDonCatchLeftTime - Time.fixedDeltaTime;
            }
            if (craneDonCatchLeftTime < 0 && !donCatchCount)
            {
                //catchPlayer = false;
                craneanim.SetBool("grab", true);
                catchNow = false;
                curCraneCatchSync = curCraneCatchSync - Time.deltaTime;
                if (curCraneCatchSync < 0)
                {
                    StartCoroutine(CraneUp());
                }
                yield return null;
            }
        }
        //carryObject.transform.position = carryPoint.transform.position;

    }
    IEnumerator CraneUp()
    {
        if (!isCraneMove)
        {
            sfx_craneAudio.clip = sfx_craneMove;
            sfx_craneAudio.loop = true;
            sfx_craneAudio.Play();
            isCraneMove = true;
        }
        Vector3 curPos = crane.transform.position; //현재 포지션.
        Vector3 upPos = craneUpPos.transform.position;//올라갈 포지션
        //print("현재포지션 : " + curPos.y);
        //print("올라갈 포지션 : " + upPos.y);

        // craneCatchAndUp이 true일때
        if (craneCatchAndUp)
        {
            if (Vector3.Distance(new Vector3(curPos.x, curPos.y, curPos.z), new Vector3(curPos.x, upPos.y, curPos.z)) > 0.2f)
            {
                crane.transform.position = Vector3.Lerp(new Vector3(curPos.x, curPos.y, curPos.z), new Vector3(curPos.x, upPos.y, curPos.z), 1 * Time.fixedDeltaTime);
                //print("잡고 올라가는중");
            }
            else if (Vector3.Distance(new Vector3(curPos.x, curPos.y, curPos.z), new Vector3(curPos.x, upPos.y, curPos.z)) < 3f)
            {
                //print("잡은채로 다 올라옴");
                crane.transform.position = new Vector3(curPos.x, upPos.y, curPos.z);
                craneHome = true;
                yield return craneHome;
            }
        }
        // craneCatchAndUp이 false일때
        else
        {
            if (Vector3.Distance(new Vector3(curPos.x, curPos.y, curPos.z), new Vector3(curPos.x, upPos.y, curPos.z)) > 0.2f)
            {
                crane.transform.position = Vector3.Lerp(new Vector3(curPos.x, curPos.y, curPos.z), new Vector3(curPos.x, upPos.y, curPos.z), 1 * Time.fixedDeltaTime);
                //print("못잡고 올라가는중");
            }
            else if (Vector3.Distance(new Vector3(curPos.x, curPos.y, curPos.z), new Vector3(curPos.x, upPos.y, curPos.z)) < 3f)
            {
                //print("못잡은채로 다 올라옴");
                crane.transform.position = new Vector3(curPos.x, upPos.y, curPos.z);
                craneHome = true;
                donCatchCount = true;
                yield return craneHome;
            }
        }
    }
    IEnumerator CraneGoHome() //잡은경우 홈으로 간뒤 놓음실행
    {
        Vector3 curpos = transform.position; //현재 x,z위치
        Vector3 homepos = craneHomePos.transform.position;//가야할 x,z위치
        if (craneCatchAndUp)
        {
            if (Vector3.Distance(new Vector3(curpos.x, curpos.y, curpos.z), new Vector3(homepos.x, curpos.y, homepos.z)) > 0.2f)
            {
                //print("잡은채로 홈으로 이동중");
                transform.position = Vector3.Lerp(new Vector3(curpos.x, curpos.y, curpos.z), new Vector3(homepos.x, curpos.y, homepos.z), 1 * Time.fixedDeltaTime);
            }
            else if (Vector3.Distance(new Vector3(curpos.x, curpos.y, curpos.z), new Vector3(homepos.x, curpos.y, homepos.z)) < 2f)
            {
                //print("잡은채로 홈이동 완료");
                transform.position = new Vector3(homepos.x, curpos.y, homepos.z);
                craneanim.SetBool("throwing", true);
                craneThrow = true; //Execution Throwing;
            }
        }
        else //못잡은경우 홈 이동 후 바로 다음사이클(초기화)
        {
            if (Vector3.Distance(new Vector3(curpos.x, curpos.y, curpos.z), new Vector3(homepos.x, curpos.y, homepos.z)) > 0.2f)
            {
                //("못잡은채로 홈으로 이동중");
                transform.position = Vector3.Lerp(new Vector3(curpos.x, curpos.y, curpos.z), new Vector3(homepos.x, curpos.y, homepos.z), 1 * Time.fixedDeltaTime);
            }
            else if (Vector3.Distance(new Vector3(curpos.x, curpos.y, curpos.z), new Vector3(homepos.x, curpos.y, homepos.z)) < 2f)
            {
                //print("못잡은 채로 홈이동 완료");
                transform.position = new Vector3(homepos.x, curpos.y, homepos.z);
                craneanim.SetBool("throwing", true);
                StartCoroutine(CraneThrowing()); //행동 종료 ->Next Cycle
            }
        }
        yield return null;
    }
    IEnumerator CraneThrowing() //초기화
    {
        //print("놓아라");
        catchCount = false;
        donCatchCount = false;
        if (catchNow) //플레이어 잡기 성공
        {
            craneanim.SetBool("wait", false);
            craneanim.SetBool("grab", false);
            craneanim.SetBool("throwing", true);
            sfx_craneAudio.clip = sfx_CraneSuccess;
            sfx_craneAudio.loop = false;
            sfx_craneAudio.Play();
            photonView.RPC("PlayerThrowing", PhotonTargets.All);
            craneState = true;
            photonView.RPC("CraneStateSynchro", PhotonTargets.All, this.photonView.viewID, 1);
            craneOff = true;
        }
        else //잡기 실패
        {
            craneanim.SetBool("wait", false);
            craneanim.SetBool("grab", false);
            craneanim.SetBool("throwing", true);
            sfx_craneAudio.clip = sfx_CraneFail;
            sfx_craneAudio.loop = false;
            sfx_craneAudio.Play();
            isCraneMove = false;
            isCraneDown = false;

            craneState = false;
            craneCatchAndUp = false;
            photonView.RPC("CraneStateSynchro", PhotonTargets.All, this.photonView.viewID, 2);
        }
        craneMoving = false;
        craneDown = false;
        catchPlayer = false;
        craneHome = false;
        catchNow = false;
        craneThrow = false;
        moveLeftTime = moveStartTime;
        craneDonCatchLeftTime = craneDonCatchStartTime;
        craneCatchLeftTime = craneCatchStartTime;
        curCraneCatchSync = craneCatchSync;
        pickPlayer = null;
        yield return null;
    }
    //private void OnGUI()
    //{
    //    if (moveLeftTime > 0 && PhotonNetwork.isMasterClient)
    //    {
    //        GUILayout.Label("크레인" + moveLeftTime.ToString() + " :초 후 이동");
    //    }
    //    if (catchNow)
    //    {
    //        GUILayout.Label("플레이어 들어옴. " + craneCatchLeftTime.ToString() + " :초 후 올라감");
    //    }
    //    else if (!catchNow)
    //    {
    //        GUILayout.Label("플레이어 못잡았다. " + craneDonCatchLeftTime.ToString() + " :초 후 올라감");
    //    }
    //}
    void CraneState()
    {
        //print("craneMoving: "+craneMoving);
        //print("craneDown: "+craneDown);
        //print("catchPlayer: "+ catchPlayer);
        //print("craneCatchAndUp :"+ craneCatchAndUp);
        //print("craneHome: "+ craneHome);
        //print("catchNow : "+ catchNow);

        if (moveLeftTime > 0 && !craneState && PhotonNetwork.isMasterClient)
        {
            moveLeftTime = moveLeftTime - Time.fixedDeltaTime;
        }
        else if (moveLeftTime < 0 && !craneState && PhotonNetwork.isMasterClient) //마스터가 움직일 포인트를 모두에게 넘겨줌.
        {
            //StartCoroutine(GoRandomPosition()); //이동 포지션선택 Moving = true
            positionpoint = Random.Range(0, movePoint.Length);
            photonView.RPC("SendGoPosition", PhotonTargets.All, positionpoint);
        }
        else if (craneMoving && !craneDown)
        {
            // print("무빙 코루틴");
            StartCoroutine(CraneMove());//이동 완료 ->Moving = false
                                        //크레인 내려야함 ->Down = true
        }
        else if (craneDown && !craneMoving)
        {
            // print("내림 코루틴");
            StartCoroutine(CraneDown()); //크레인 내림 완료후 Down = false
        }
        else if (!craneMoving && !craneDown && catchPlayer) //잡는중 cachPlayer = true //못잡게 하려면 false
        {                                                   //catchNow = true
                                                            //print("잡음 코루틴");
            StartCoroutine(CraneCatch());
        }
        //////
        if (catchNow && craneCatchAndUp && !craneHome)
        {
            //print("올림 코루틴");
            StartCoroutine(CraneUp());
        }
        else if (craneHome && !craneThrow)
        {
            //print("홈 이동 코루틴");
            StartCoroutine(CraneGoHome());
        }
        if (craneThrow)
        {
            StartCoroutine(CraneThrowing());
        }
    }
    [PunRPC]
    void PlayerCatch(int id)
    {
        craneanim.SetBool("grab", true);
        holdPlayer = PhotonView.Find(id).gameObject;
        holdPlayer.GetComponent<HoldState>().craneHolding = true;
        holdPlayer.transform.parent = pickPoint.transform;
        holdPlayer.GetComponent<HoldState>().iWasCaught = true;
        holdPlayer.GetComponentInChildren<Animator>().SetBool("IwasCaught", true);
        holdPlayer.GetComponent<CharacterController>().enabled = false;
        holdPlayer.transform.localPosition = Vector3.zero;
        holdPlayer.transform.rotation = Quaternion.LookRotation(pickPoint.transform.forward);
    }
    [PunRPC]
    void PlayerThrowing()
    {
        holdPlayer.GetComponent<CharacterController>().enabled = true;
        holdPlayer.GetComponentInChildren<Animator>().SetBool("IwasCaught", false);
        holdPlayer.GetComponent<HoldState>().iWasCaught = false;
        pickPoint.transform.DetachChildren();
    }
    [PunRPC]
    void CraneStateSynchro(int id, int state)
    {
        switch (state)
        {
            case 1:
                {
                    PhotonView.Find(id).gameObject.GetComponent<CraneCtrl>().craneState = true;
                    break;
                }
            case 2:
                {
                    PhotonView.Find(id).gameObject.GetComponent<CraneCtrl>().craneState = false;
                    break;
                }
        }
    }
}
