﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillUI : MonoBehaviour
{
    [HideInInspector]public PlayerManager target;
    public static SkillUI ins;
    public Image blindUI;
    public Image fastUI;

    private Image blindCooltimeFill;
    private Image fastCooltimeFill;
    private Text blindCooltimeText;
    private Text fastCoolTimeText;

    int skill;
    bool isdoll;

    public Image reaperScanUI;
    public Image reaperBlindUI;

    private Image reaperScanUIFill;
    private Image reaperBlindFill;
    private Text reaperBlindUICooltimeText;
    private Text reaperScanUIfastCoolTimeText;


    // Use this for initialization
    private void Awake()
    {
        ins = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            if (isdoll)
            {
                if (skill == 0)
                {
                    blindCooltimeFill.fillAmount = target.blindSkillCurTime / target.blindSkillCoolTime;
                    blindCooltimeText.text = target.blindSkillCurTime.ToString("N0");
                    if (target.blindSkillCurTime == target.blindSkillCoolTime)
                    {
                        blindCooltimeFill.gameObject.SetActive(false);
                        blindCooltimeText.gameObject.SetActive(false);
                    }
                    else
                    {
                        blindCooltimeFill.gameObject.SetActive(true);
                        blindCooltimeText.gameObject.SetActive(true);
                    }
                }
                else if (skill == 1)
                {
                    fastCooltimeFill.fillAmount = target.fastSkillCurTime / target.fastSkillCoolTime;
                    fastCoolTimeText.text = target.fastSkillCurTime.ToString("N0");
                    if (target.blindSkillCurTime == target.blindSkillCoolTime)//fastCooltimeFill.fillAmount == 1
                    {
                        fastCooltimeFill.gameObject.SetActive(false);
                        fastCoolTimeText.gameObject.SetActive(false);
                    }
                    else
                    {
                        fastCooltimeFill.gameObject.SetActive(true);
                        fastCoolTimeText.gameObject.SetActive(true);
                    }
                }
            }
            else
            {
                //print(reaperBlindUICooltimeText.text + "Blind Cool");
                //print(reaperScanUIfastCoolTimeText.text + "Scan Cool");
                reaperBlindFill.fillAmount = target.gameObject.GetComponent<Blood_Deadman>().leftTime_Blood /
                    target.gameObject.GetComponent<Blood_Deadman>().coolTime_Blood;
                reaperBlindUICooltimeText.text = target.gameObject.GetComponent<Blood_Deadman>().leftTime_Blood.ToString("N0");
                reaperScanUIFill.fillAmount = target.gameObject.GetComponent<Dead_Scan>().leftTime_Scan /
                    target.gameObject.GetComponent<Dead_Scan>().coolTime_Scan;
                reaperScanUIfastCoolTimeText.text = target.gameObject.GetComponent<Dead_Scan>().leftTime_Scan.ToString("N0");
                if (reaperScanUIFill.fillAmount == 1)
                {
                    reaperScanUIFill.gameObject.SetActive(false);
                    reaperScanUIfastCoolTimeText.gameObject.SetActive(false);
                }
                else
                {
                    reaperScanUIFill.gameObject.SetActive(true);
                    reaperScanUIfastCoolTimeText.gameObject.SetActive(true);
                }
                if (reaperBlindFill.fillAmount == 1)
                {
                    reaperBlindFill.gameObject.SetActive(false);
                    reaperBlindUICooltimeText.gameObject.SetActive(false);
                }
                else
                {
                    reaperBlindFill.gameObject.SetActive(true);
                    reaperBlindUICooltimeText.gameObject.SetActive(true);
                }
            }

        }
    }
    public void SetTarget(PlayerManager _target)
    {
        if (_target == null)
        {
            return;
        }
        else
        {
            target = _target;
            if (target.gameObject.CompareTag("Doll"))
            {
                isdoll = true;
                reaperScanUI.gameObject.SetActive(false);
                reaperBlindUI.gameObject.SetActive(false);
                if (Exit_DBAPI.instance.data.setSkill == 0)
                {
                    skill = Exit_DBAPI.instance.data.setSkill;
                    blindUI.gameObject.SetActive(true);
                    fastUI.gameObject.SetActive(false);
                    blindCooltimeFill = blindUI.transform.GetChild(0).GetComponent<Image>();
                    blindCooltimeText = blindUI.transform.GetComponentInChildren<Text>();
                }
                else if (Exit_DBAPI.instance.data.setSkill == 1)
                {
                    skill = Exit_DBAPI.instance.data.setSkill;
                    blindUI.gameObject.SetActive(false);
                    fastUI.gameObject.SetActive(true);
                    fastCooltimeFill = fastUI.transform.GetChild(0).GetComponent<Image>();
                    fastCoolTimeText = fastUI.GetComponentInChildren<Text>();
                }
            }
            else if (target.gameObject.CompareTag("Dead"))
            {
                isdoll = false;
                print("Your Reaper");
                blindUI.gameObject.SetActive(false);
                fastUI.gameObject.SetActive(false);
                reaperScanUI.gameObject.SetActive(true);
                reaperBlindUI.gameObject.SetActive(true);
                reaperBlindFill = reaperBlindUI.transform.GetChild(0).GetComponent<Image>();
                reaperBlindUICooltimeText = reaperBlindUI.GetComponentInChildren<Text>();
                reaperScanUIFill = reaperScanUI.transform.GetChild(0).GetComponent<Image>();
                reaperScanUIfastCoolTimeText = reaperScanUI.GetComponentInChildren<Text>();
            }
        }
    }

}
;