﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class FlashLight : MonoBehaviour
{
    public float intensity;
    Renderer rend;
    public Color mycolor;
    float startT;
    float curStartT;
    bool setT;
    float stIntensity;

    // Use this for initialization
    void Start()
    {
        rend = GetComponent<Renderer>();
        StartCoroutine(FlashRandom());
        stIntensity = intensity;
    }

    // Update is called once per frame
    void Update()
    {
        rend.material.SetColor("_EmissionColor", mycolor * intensity);
        DynamicGI.SetEmissive(rend, mycolor * intensity);
    }
    IEnumerator FlashRandom()
    {
        if (setT == false)
        {
            startT = Random.Range(0.7f, 1.5f);
            curStartT = startT;
            setT = true;
        }

        yield return new WaitForSeconds(curStartT);

        if (intensity == 0)
        {
            //print("ON");
            setT = false;
            intensity = stIntensity;
        }
        else if (intensity == stIntensity)
        {
            //print("OFF");
            setT = false;
            intensity = 0f;
        }
        StartCoroutine(FlashRandom());
        yield return null;
    }
}