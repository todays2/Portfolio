﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]

public class TileMap_KS : MonoBehaviour {
    public int size_x = 100;
    public int size_z = 50;
    public float tileSize = 1.0f;
    Transform center;
    //public GameObject insTile;
    public Transform tilePrefab;
   public static Vector2 mapSize;

    private void Awake()
    {
        mapSize.x = size_x;
        mapSize.y = size_z;
    }
    // Use this for initialization
    void Start () {
        center = this.transform;
        BuildMesh();
        //print("" +(-size_x / 2f) * (tileSize * 10f));
        //insTile.transform.localScale = new Vector3(tileSize*10, 0.2f, tileSize*10);
	}
    
    public void BuildMesh()
    {
        size_x += 1;
        size_z += 1;
        center.transform.position = new Vector3(((-size_x+1f) / 2f) * (tileSize*10f), this.transform.position.y,((-size_z+1f) / 2f) * (tileSize*10f));
        int numTiles = size_x * size_z;
        int numTriangles = numTiles * 2;
        int tileposition = size_x - 5;
        int vSize_x = size_x + 1;
        int vSize_z = size_z + 1;
        int numVerties = vSize_x * vSize_z;

        Vector3[] verties = new Vector3[numVerties];
        Vector3[] normals = new Vector3[numVerties];
        Vector2[] uv = new Vector2[numVerties];

        int[] triangles = new int[numTriangles *3];

        int x, z, t;
        for(z = 0; z < size_z; z++)
        {
            for (x = 0; x < size_x; x++)
            {
                t = z * vSize_x + x;
                verties[t] = new Vector3(x * tileSize, 0, z * tileSize);
                normals[t] = Vector3.up;
                uv[t] = new Vector2((float)x / vSize_x, (float)z / vSize_z);
                //Instantiate(insTile,new Vector3(this.transform.position.x+(verties[0].x+tileposition+(x*10)),1,this.transform.position.x+(verties[0].x+tileposition+(x*10))),Quaternion.identity);
            }
        }
        for(z = 0; z < size_z; z++)
        {
            for(x = 0; x < size_x; x++)
            {
                int triOffset = (z * size_x + x) * 6;
                t = z * vSize_x + x;
                triangles[triOffset + 0] = t + 0;
                triangles[triOffset + 1] = t + vSize_x + 0;
                triangles[triOffset + 2] = t + vSize_x + 1;

                triangles[triOffset + 3] = t + 0;
                triangles[triOffset + 4] = t + vSize_x + 1;
                triangles[triOffset + 5] = t + 1;

            }
        }
        
        Mesh mesh = new Mesh();
        mesh.vertices = verties;
        mesh.triangles = triangles;
        mesh.normals = normals;
        mesh.uv = uv;

        MeshFilter meshFilter = GetComponent<MeshFilter>();
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        MeshCollider meshColider = GetComponent<MeshCollider>();

        meshFilter.mesh = mesh;
        meshColider.sharedMesh = mesh;
        
        this.transform.position = center.transform.position;

        size_x -= 1;
        size_z -= 1;





       
    }

    // Update is called once per frame
    void Update () {
		
	}
}
