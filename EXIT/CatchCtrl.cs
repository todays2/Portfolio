﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace UnityStandardAssets.Characters.FirstPerson
{
    public class CatchCtrl : MonoBehaviour
    {
        PhotonView pv;
        public Transform carryPoint; //잡을 위치 (NPC)
        public Transform playerPoint;//잡을 위치(Player)

        Collider[] items; //닿은 오브젝트들
        Collider[] filters; //닿은 오브젝트중 걸러냄
       
        GameObject rpcPlayer; //네트워크 -행동하는 플레이어
        GameObject rpcObject; //네트워크 -당하는 오브젝트 (가까운)

        Vector3 nockbackDir; //넉백 방향
        Vector3 caughtDir; //잡힌 방향
        int caughtID; //자신의 ID    
        int itemID; //가까운 오브젝트의 ID
        int index = 0; //제일 가까운 오브젝트의 인덱스

        public bool nockback; //넉백 중인지
        bool push; //true일 경우 밀쳐짐
        bool npcHolding; //true일 경우 스테미너 --
        bool playerHolding; //true일 경우 스테미너 --
        bool imstun; //내가 스턴중인지
        public bool dollCatchCT; //플레이어 잡는 쿨타임중인지
        float catchCT = 5f; //플레이어 잡는 쿨타임 초기화용
        float catchCurCT; //플레이어 잡는 쿨타임
        bool isholding; //잡고있는지
        float npcPushTime = 1f; //NPC 넉백용 타임
        float playerPushTime = 0.2f; //플레이어 넉백용 타임
        float nockbackPower = 5f; //넉백 힘
        float stunCurTime;
        public float stunTime = 2f; //넉백 후 스턴 타임
        float pushCurTime; //밀리는 시간
        float[] dists; //닿은 오브젝트들의 거리정보
        int ids;
        bool dic;
        Animator anim;


        private void Start()
        {
            rpcPlayer = this.gameObject;
            filters = new Collider[100];
            dists = new float[100];
            stunCurTime = stunTime;
            catchCurCT = catchCT;
            anim = GetComponent<Animator>();
            pv = GetComponent<PhotonView>();
        }
        // Update is called once per frame

        private void FixedUpdate()
        {
            //if (Exit_GameManager.instance.craneMove && !dic)
            //{
            //    //Exit_GameManager.instance.players.Add(PhotonNetwork.player.ID, this.gameObject);
            //    pv.RPC("AddDictionary", PhotonTargets.All, pv.viewID, PhotonNetwork.player.ID);
            //}

            if (pv.isMine) //자신의 것만 처리함
            {
                HoldState(); //잡기 로직
                HealthState(); //스테미너 로직
                 //잡혀있거나, 스턴일 경우 움직이지 못하게
                if (this.gameObject.GetComponent<HoldState>().iWasCaught ||
                    this.gameObject.GetComponent<PlayerHealth>().stun)
                {
                    //print("컨트롤러 Off");
                    this.gameObject.GetComponent<FirstPersonController>().enabled = false;
                }//잡혀있지 않으면 움직일 수 있도록
                else if (!this.gameObject.GetComponent<HoldState>().iWasCaught)
                {
                    //print("컨트롤러 ON");
                    this.gameObject.GetComponent<FirstPersonController>().enabled = true;
                }

            }
            //if (Input.GetKeyDown(KeyCode.G))
            //{
            //    OnLeaveRoom();
            //}
            Push(); //넉백 로직

        }
        //public void OnLeaveRoom()//무언가 잡은채로 방을 나가버리면 오브젝트를 가지고 나가버려서 사라져버림. 따라서 잡고있다면 놓게 해야함.
        //{
        //    //print("LeaveRoom");
        //    if (rpcPlayer.GetComponent<CatchCtrl>().carryPoint.GetChildCount() > 0)
        //    {
        //        pv.RPC("CatchState", PhotonTargets.All, itemID, caughtID, 2);
        //    }
        //    PhotonNetwork.LeaveRoom();
        //}

        void HoldState()
        {   //자신이면서 자신의 잡음 상태가 false인경우
            if (dollCatchCT) //플레이어 잡는 쿨타임
            {
                catchCurCT = catchCurCT - Time.fixedDeltaTime;
                if (catchCurCT < 0)
                {
                    dollCatchCT = false;
                    catchCurCT = catchCT;
                }
            } //스턴이 아니며, 로딩중이 아니라면 무언가를 인식 가능.
            if (pv.isMine && !rpcPlayer.GetComponent<PlayerHealth>().stun && !Exit_GameManager.instance.loading.activeSelf)
            {
                //print("잡을것 인식중");
                if (!rpcPlayer.GetComponent<HoldState>().holdNow && !rpcPlayer.GetComponent<HoldState>().iWasCaught)
                {   //CatchItem Layer인 오브젝트들 받아옴(자기 자신도 포함)
                    items = Physics.OverlapSphere(carryPoint.transform.position, 0.25f, 1 << LayerMask.NameToLayer("CatchItem"),
                                                                                                QueryTriggerInteraction.Ignore);
                    index = 0; //매 검사마다 초기화
                    if (items != null)
                    {
                        for (int i = 0, k = 0; i < items.Length; i++) //닿은 오브젝트들 걸러내기
                        {
                            //print("닿은 아이템들 : " + items[i].name + "Num : " + i);
                            //자신이 아니며 누군가가 들고있는 오브젝트가 아닌것들의 자신과의 사이거리 체크
                            if (items[i].gameObject != this.gameObject && !items[i].GetComponent<HoldState>().iWasCaught)
                            {
                                filters[k] = items[i];
                                //print("필터 : " + filters[k].name+" " +k+" 번째 " +"거리 : "+dists[k]);
                                dists[k] = Vector3.Distance(this.gameObject.transform.position, filters[k].transform.position);
                                k++;
                            }
                        }
                        float min = dists[0]; //매 검사마다 초기화

                        for (int i = 0; i < items.Length - 1; i++)
                        { //거리중 가장 가까운 것의 인덱스 알아냄.
                            if (min > dists[i])
                            {
                                index = i;
                                min = dists[index];
                            }
                        }
                        //print(min);
                        //print("index : " + index);
                        //자신의 잡음 상태가 false이며 자신이외의 닿은 오브젝트가 더 있다면
                        if (Input.GetButton("Fire1") && !rpcPlayer.GetComponent<HoldState>().holdNow && items.Length >= 2 && !isholding && !dollCatchCT)
                        {
                            isholding = true;
                            rpcObject = filters[index].gameObject; //가까운 오브젝트를 잡음                        
                            itemID = rpcObject.gameObject.GetComponent<PhotonView>().viewID; //가까운 오브젝트의 ID를 받아옴
                            caughtID = transform.gameObject.GetComponent<PhotonView>().viewID; //자신 ID를 받아옴                
                            //잡은 오브젝트가 무언가 잡고있다면(잡을 오브젝트가 플레이어이며 무언가 들고있다면)
                            if (rpcObject.CompareTag("Doll") && rpcObject.GetComponent<CatchCtrl>().playerPoint.GetChildCount() > 0)
                            {
                                //print("내려놓게하기");
                                pv.RPC("CatchState", PhotonTargets.All, itemID, caughtID, 3);
                            }else if(rpcObject.CompareTag("Doll") && rpcObject.GetComponent<CatchCtrl>().carryPoint.GetChildCount() > 0)
                            {
                                pv.RPC("CatchState", PhotonTargets.All, itemID, caughtID, 3);
                            }
                            //print("가까이 닿은 아이템 : " + rpcObject);                    
                            pv.RPC("CatchState", PhotonTargets.All, itemID, caughtID, 1);
                        }
                    }
                }//자신의 잡음상태가 true이고 잡은 오브젝트의 잡힘상태가 true이면
                //놓기                                                                                //ESC눌러서 팝업되는 창을 띄우면
                if (!Input.GetButton("Fire1") && !push && !Input.GetKey(KeyCode.E) && isholding || (Exit_GameManager.instance.escPopup.activeSelf && isholding))
                {
                    isholding = false;
                    pv.RPC("CatchState", PhotonTargets.All, rpcObject.GetComponent<PhotonView>().viewID, caughtID, 2);
                    transform.GetComponentInChildren<Animator>().SetBool("Catch", false);
                }
                //밀치기
                if (Input.GetKey(KeyCode.E) && Input.GetButtonUp("Fire1") && rpcPlayer.GetComponent<HoldState>().holdNow &&
                                                                             rpcObject.GetComponent<HoldState>().iWasCaught)
                {
                    isholding = false;
                    //photonView.RPC("CatchState", PhotonTargets.All, rpcObject.GetComponent<PhotonView>().viewID, caughtID, 2);
                    pv.RPC("CatchState", PhotonTargets.All, rpcObject.GetComponent<PhotonView>().viewID, caughtID, 4);
                    transform.GetComponentInChildren<Animator>().SetBool("Catch", false);
                }
            }
        }
        void Push()
        {
            if (push && rpcObject != null)
            {
                //print("NPC밀치고있음");
                pushCurTime = pushCurTime - Time.fixedDeltaTime;
                if (0 > pushCurTime)
                {
                    //print("움직여라");
                    push = false;
                    rpcObject.GetComponent<Rigidbody>().isKinematic = true;
                    rpcObject.GetComponent<Rigidbody>().isKinematic = false;
                }
            }
            else if (nockback)
            {
                //print("Doll밀침");
                GetComponent<CharacterController>().Move(nockbackDir * nockbackPower * Time.fixedDeltaTime);
                StartCoroutine(NockbackEnd());
            }
        }
        void HealthState()
        {

            if (rpcPlayer.GetComponent<HoldState>().holdNow && !rpcPlayer.GetComponent<HoldState>().iWasCaught) //들고있으면 에너지 감소
            {
                if (rpcObject.CompareTag("NPC")) //NPC들고있다면
                {
                    rpcPlayer.GetComponent<PlayerHealth>().health = rpcPlayer.GetComponent<PlayerHealth>().health - (15 * Time.fixedDeltaTime);
                    rpcObject.transform.localPosition = Vector3.zero;
                    rpcObject.transform.localRotation = Quaternion.LookRotation(new Vector3(0, 90f, 0));
                }
                else if (rpcObject.CompareTag("Doll") && dollCatchCT == false)//Doll 들고있다면
                {
                    rpcPlayer.GetComponent<PlayerHealth>().health = rpcPlayer.GetComponent<PlayerHealth>().health - (40 * Time.fixedDeltaTime);
                    //print("잡고있다 : 피"+rpcPlayer.GetComponent<PlayerHealth>().health);
                }
            } //들고있지 않으면 에너지 회복
            else if (!rpcPlayer.GetComponent<HoldState>().holdNow &&
                      rpcPlayer.GetComponent<PlayerHealth>().health < 100 ||
                      rpcPlayer.GetComponent<HoldState>().iWasCaught || dollCatchCT)
            {
                rpcPlayer.GetComponent<PlayerHealth>().health = rpcPlayer.GetComponent<PlayerHealth>().health + (5 * Time.fixedDeltaTime);
            } //100이상 되지 않도록
            if (rpcPlayer.GetComponent<PlayerHealth>().health > 100)
            {
                rpcPlayer.GetComponent<PlayerHealth>().health = 100;
            } //에너지 전부 소모하면 스턴
            else if (rpcPlayer.GetComponent<PlayerHealth>().health < 5 && !rpcPlayer.GetComponent<PlayerHealth>().stun)
            {
                //print("스턴걸림");
                stunTime = 5f;
                rpcPlayer.GetComponent<PlayerHealth>().stun = true;
                //rpcPlayer.GetComponentInChildren<Animator>().SetBool("stun", true);
                //rpcPlayer.GetComponent<FirstPersonController>().enabled = false;
                pv.RPC("DollCatchAnimationSycn", PhotonTargets.All, rpcPlayer.GetComponent<PhotonView>().viewID, 1);
                if (rpcPlayer.GetComponent<HoldState>().holdNow)
                {
                    pv.RPC("CatchState", PhotonTargets.All, itemID, caughtID, 5);
                }
            } //스턴 걸린후 x초뒤 스턴회복
            if (rpcPlayer.GetComponent<PlayerHealth>().stun || this.GetComponent<PlayerHealth>().stun)
            {
                //print("스턴풀기");
                if (!imstun)
                {
                    stunCurTime = stunTime;
                    imstun = true;
                }
                stunCurTime = stunCurTime - Time.fixedDeltaTime;
                if (0 > stunCurTime)
                {
                    rpcPlayer.GetComponent<PlayerHealth>().stun = false;
                    rpcPlayer.GetComponentInChildren<Animator>().SetBool("stun", false);
                    //rpcPlayer.GetComponent<FirstPersonController>().enabled = false;
                    pv.RPC("DollCatchAnimationSycn", PhotonTargets.All, rpcPlayer.GetComponent<PhotonView>().viewID, 2);
                    stunCurTime = stunTime;
                    imstun = false;
                }
            }
        }
        [PunRPC]
        void NockBack(Vector3 nockdir, int id)
        {
            ids = id;
            PhotonView.Find(id).gameObject.GetComponent<CatchCtrl>().nockbackDir = nockdir;
            PhotonView.Find(id).gameObject.GetComponent<CatchCtrl>().nockback = true;
        }
        [PunRPC]
        void Caught(Vector3 dir, int id)
        {
            PhotonView.Find(id).gameObject.GetComponent<CatchCtrl>().caughtDir = dir;
        }
        IEnumerator NockbackEnd()
        {
            yield return new WaitForSeconds(playerPushTime);
            print("미");
            nockback = false;
        }
        [PunRPC]    //state 1 = Hold, 2 = throwing, 3 = Discarding the holding object, 4 = Push Object, 5 = Stun
        void CatchState(int _itemsID, int _caughtID, int state)
        {
            rpcObject = PhotonView.Find(_itemsID).gameObject; //잡을 오브젝트
            rpcPlayer = PhotonView.Find(_caughtID).gameObject; //행동하는 플레이어
            switch (state)
            {
                case 1:
                    { // 인형을 잡고 놓았을경우엔 다음에 다시 잡기위해선 쿨타임이 필요하므로 NPC와 Doll의 상태를 따로해줘야함
                        // 쿨타임 중 NPC는 잡을수 있음
                        //print("잡기");
                        //print("ob : " + rpcObject.name);
                        //print("PL : " + rpcPlayer.name);
                        //print("Ob Caught : " + rpcObject.GetComponent<HoldState>().iWasCaught);
                        //print("pl Caught : " + rpcPlayer.GetComponent<HoldState>().iWasCaught);
                        //print("pl hold: " + rpcPlayer.GetComponent<HoldState>().holdNow);
                        //(내가 들고있지 아니하고 잡혀있지아니하며 잡을오브젝트도 잡혀있지 아니한 상태
                        //잡을 오브젝트의 잡힘상태가 false이고 자신의 잡힘상태가 false인경우 이며 자신의 잡음상태가 false인경우에만
                        if (!rpcObject.GetComponent<HoldState>().iWasCaught && !rpcPlayer.GetComponent<HoldState>().iWasCaught &&
                            !rpcPlayer.GetComponent<HoldState>().holdNow)
                        {
                            //print("잡을 수 있음");
                            //잡은오브젝트가 NPC일경우
                            if (rpcObject.gameObject.CompareTag("NPC"))
                            {
                                rpcObject.GetComponent<HoldState>().iWasCaught = true; //오브젝트의 잡힘상태를 True
                                rpcPlayer.GetComponent<HoldState>().holdNow = true; //자신의 잡음상태 true          
                                rpcObject.transform.parent = rpcPlayer.GetComponent<CatchCtrl>().carryPoint.transform;//자식으로                            
                                rpcObject.transform.localPosition = Vector3.zero;
                                rpcPlayer.GetComponentInChildren<Animator>().SetBool("Catch", true);
                                rpcObject.GetComponent<Rigidbody>().isKinematic = true;//잡은 오브젝트의 키네마틱
                                rpcObject.GetComponent<Rigidbody>().useGravity = false;//중력
                                rpcObject.transform.localRotation = Quaternion.LookRotation(new Vector3(0, 90f, 0));
                                if (rpcObject.GetPhotonView().ownerId != rpcPlayer.GetPhotonView().ownerId) //오브젝트 소유권이 없다면 소유권을 가져옴.
                                {
                                    //print("소유권양도");
                                    rpcObject.GetPhotonView().RequestOwnership();
                                }
                                else if (rpcObject.GetPhotonView().ownerId == rpcPlayer.GetPhotonView().ownerId) //이미 소유권이있다면
                                {
                                    return;
                                }
                                //print("NPC잡았다");
                            }
                            //잡은오브젝트가 플레이어일 경우 (자신이 잡혀있는 상태가 아니라면)
                            else if (rpcObject.gameObject.CompareTag("Doll"))
                            {
                                //print("Doll잡았다");
                                rpcPlayer.GetComponentInChildren<Animator>().SetBool("Catch", true);
                                rpcObject.transform.parent = rpcPlayer.GetComponent<CatchCtrl>().playerPoint.transform;//자식으로                            
                                rpcObject.transform.localPosition = Vector3.zero;
                                rpcObject.transform.rotation = Quaternion.LookRotation(rpcPlayer.GetComponent<CatchCtrl>().carryPoint.transform.forward);
                                rpcObject.GetComponent<HoldState>().iWasCaught = true; //오브젝트의 잡힘상태를 True
                                rpcPlayer.GetComponent<HoldState>().holdNow = true; //자신의 잡음상태 true          
                                rpcObject.GetComponent<HoldState>().holdNow = true; //다른플레이어의 들고있음 true
                                rpcObject.GetComponent<CharacterController>().enabled = false;
                                rpcObject.GetComponent<PlayerNetWorkTest>().enabled = false;
                                rpcObject.GetComponentInChildren<Animator>().SetBool("Catched", true); //잡힌놈
                                rpcObject.GetComponent<CatchCtrl>().stunTime = 2f;
                                //rpcObject 무브 메소드 false
                            }
                        }
                        break;
                    }
                case 2:
                    {
                        if (rpcObject.GetComponent<HoldState>().iWasCaught && !rpcPlayer.GetComponent<HoldState>().iWasCaught &&
                            rpcPlayer.GetComponent<HoldState>().holdNow)
                        {   //잡은오브젝트가 NPC일경우
                            //print("버리기");
                            rpcObject.GetComponent<HoldState>().iWasCaught = false; //잡힘상태 false                    
                            rpcPlayer.GetComponent<HoldState>().holdNow = false; //자신의 잡음상태 false
                            rpcPlayer.GetComponentInChildren<Animator>().SetBool("Catch", false);
                            if (rpcObject.gameObject.CompareTag("NPC"))
                            {
                                rpcPlayer.GetComponent<CatchCtrl>().carryPoint.DetachChildren();//자식버림
                                rpcObject.GetComponent<Rigidbody>().isKinematic = false;//잡힌놈 키네마틱
                                rpcObject.GetComponent<Rigidbody>().useGravity = true; //중력

                                //print("NPC버렸다");
                            }
                            //잡은오브젝트가 플레이어일 경우 (자신이 잡혀있는 상태가 아니라면)
                            else if (rpcObject.gameObject.CompareTag("Doll"))
                            {
                                //print("Doll버렸다");
                                rpcObject.transform.rotation = Quaternion.identity;
                                rpcPlayer.GetComponent<CatchCtrl>().playerPoint.DetachChildren();//자식버림
                                rpcPlayer.GetComponent<CatchCtrl>().dollCatchCT = true;
                                rpcObject.GetComponent<HoldState>().holdNow = false; //다른플레이어의 들고있음 false
                                rpcObject.GetComponent<CharacterController>().enabled = true;
                                //rpcObject 무브 메소드 true                           
                                //rpcObject.GetComponent<FirstPersonController>().enabled = true;
                                rpcObject.GetComponent<PlayerNetWorkTest>().enabled = true;
                                rpcObject.GetComponentInChildren<Animator>().SetBool("Catched", false);
                            }
                        }
                        break;
                    }
                case 3:  //잡은 오브젝트가 잡은 오브젝트 잡힘상태,중력,키네마틱 ,자식버림
                    {
                        //print("잡고있는것을 내려놓게나");
                        rpcObject.GetComponent<CatchCtrl>().isholding = false;
                        if (rpcObject.GetComponent<CatchCtrl>().carryPoint.childCount != 0 /*&& rpcObject.GetComponent<CatchCtrl>().carryPoint.GetChild(0).CompareTag("NPC")*/)
                        {
                            //print("NPC 놓았다");
                            rpcObject.GetComponent<CatchCtrl>().carryPoint.GetChild(0).GetComponent<HoldState>().iWasCaught = false;
                            rpcObject.GetComponent<CatchCtrl>().carryPoint.GetChild(0).GetComponent<Rigidbody>().isKinematic = false;
                            rpcObject.GetComponent<CatchCtrl>().carryPoint.GetChild(0).GetComponent<Rigidbody>().useGravity = true;
                            rpcObject.GetComponentInChildren<Animator>().SetBool("Catch", false);
                            rpcObject.GetComponent<CatchCtrl>().carryPoint.DetachChildren();
                        }
                        else if (rpcObject.GetComponent<CatchCtrl>().playerPoint.GetChild(0).CompareTag("Doll"))
                        {
                            //print("플레이어 놓았다");
                            rpcObject.GetComponent<CatchCtrl>().playerPoint.GetChild(0).GetComponent<HoldState>().iWasCaught = false;
                            rpcObject.GetComponentInChildren<Animator>().SetBool("Catch", false);
                            rpcObject.GetComponent<CatchCtrl>().playerPoint.GetChild(0).GetComponent<HoldState>().holdNow = false;
                            rpcObject.GetComponent<CatchCtrl>().playerPoint.GetChild(0).GetComponent<CharacterController>().enabled = true;
                            rpcObject.GetComponent<CatchCtrl>().playerPoint.GetChild(0).transform.position =
                            rpcObject.GetComponent<CatchCtrl>().playerPoint.transform.position;
                            //rpcObject 무브 메소드 true                        
                            //rpcObject.GetComponent<FirstPersonController>().enabled = true;
                            rpcObject.GetComponent<CatchCtrl>().playerPoint.GetChild(0).GetComponent<PlayerNetWorkTest>().enabled = true;
                            rpcObject.GetComponent<CatchCtrl>().playerPoint.GetChild(0).GetComponentInChildren<Animator>().SetBool("Catched", false);
                            rpcObject.GetComponent<CatchCtrl>().playerPoint.DetachChildren();
                            rpcObject.GetComponent<CatchCtrl>().dollCatchCT = true;
                        }
                        break;
                    }
                case 4:
                    {
                        float pushPower = 100000f;
                        Vector3 pushVector;
                        pushCurTime = npcPushTime;
                        rpcObject.GetComponent<HoldState>().iWasCaught = false; //잡힘상태 false                    
                        rpcPlayer.GetComponent<HoldState>().holdNow = false;    //자신의 잡음상태false
                        rpcPlayer.GetComponentInChildren<Animator>().SetBool("Catch", false);
                        rpcPlayer.GetComponentInChildren<Animator>().SetBool("Push", true);
                        StartCoroutine(PushAnimation());
                        if (rpcObject.gameObject.CompareTag("NPC"))
                        {
                            rpcPlayer.GetComponent<PlayerHealth>().health = rpcPlayer.GetComponent<PlayerHealth>().health - 5f;
                            rpcPlayer.GetComponent<CatchCtrl>().carryPoint.DetachChildren();//자식버림                        
                            //print("NPC 밀쳤다");
                            pushVector = rpcPlayer.transform.forward;
                            rpcObject.GetComponent<Rigidbody>().isKinematic = false;//잡힌놈 키네마틱
                            rpcObject.GetComponent<Rigidbody>().useGravity = true; //중력
                            rpcObject.GetComponent<Rigidbody>().AddForce(pushVector * pushPower); //밀치기
                            push = true;

                        }
                        else if (rpcObject.gameObject.CompareTag("Doll"))
                        {
                            //print("Doll 밀쳤다");
                            rpcPlayer.GetComponent<PlayerHealth>().health = rpcPlayer.GetComponent<PlayerHealth>().health - 30f;
                            rpcObject.GetComponent<CatchCtrl>().stunTime = 2f;
                            rpcObject.GetComponent<PlayerHealth>().stun = true;
                            rpcPlayer.GetComponent<CatchCtrl>().playerPoint.DetachChildren();//자식버림        
                            rpcPlayer.GetComponent<CatchCtrl>().dollCatchCT = true;
                            rpcObject.GetComponent<PlayerNetWorkTest>().enabled = true;
                            rpcObject.GetComponent<CharacterController>().enabled = true;

                            //밀치는 메소드
                            rpcObject.GetComponent<HoldState>().holdNow = false;
                            rpcObject.GetComponentInChildren<Animator>().SetBool("Catched", false);
                            rpcObject.GetComponentInChildren<Animator>().SetBool("stun", true);
                            pv.RPC("NockBack", PhotonTargets.All, transform.forward, rpcObject.GetComponent<PhotonView>().viewID);

                        }
                        break;
                    }
                case 5:
                    {
                        //print("스턴 잡고있는것을 내려놓게나");
                        rpcPlayer.GetComponent<HoldState>().holdNow = false;
                        rpcObject.GetComponent<HoldState>().iWasCaught = false;
                        rpcPlayer.GetComponent<CatchCtrl>().isholding = false;
                        if (rpcObject.gameObject.CompareTag("NPC"))
                        {
                            //print("스턴 NPC 놓았다");
                            rpcObject.GetComponent<Rigidbody>().isKinematic = false;
                            rpcObject.GetComponent<Rigidbody>().useGravity = true;
                            rpcPlayer.GetComponent<CatchCtrl>().carryPoint.DetachChildren();
                        }

                        else if (rpcObject.gameObject.CompareTag("Doll"))
                        {
                            //print("스턴 플레이어 놓았다");
                            rpcObject.GetComponent<HoldState>().holdNow = false;
                            rpcObject.GetComponent<CharacterController>().enabled = true;

                            //rpcObject 무브 메소드 true                        
                            //rpcObject.GetComponent<FirstPersonController>().enabled = true;
                            rpcObject.GetComponent<PlayerNetWorkTest>().enabled = true;
                            rpcObject.GetComponentInChildren<Animator>().SetBool("Catched", false);
                            rpcPlayer.GetComponent<CatchCtrl>().playerPoint.DetachChildren();
                            rpcPlayer.GetComponent<CatchCtrl>().dollCatchCT = true;
                        }
                        rpcPlayer.GetComponentInChildren<Animator>().SetBool("Catch", false);
                        break;
                    }
            }
        }
        [PunRPC]
        void DollExit(int id)
        {
            GameObject exitOb = PhotonView.Find(id).gameObject;
            if (exitOb.GetComponent<PhotonView>().isMine)
            {
                //print("Doll Destroyed");
                //PhotonNetwork.LeaveRoom();
                exitOb.GetComponent<FirstPersonController>().enabled = false;
                exitOb.GetComponent<CharacterController>().enabled = false;
            }
        }
        [PunRPC]
        void DollCatchAnimationSycn(int id, int state)
        {
            //print("animate");
            GameObject whostun = PhotonView.Find(id).gameObject;
            switch (state) //1 ,2 =스턴 애니매이션 동기화
            {
                case 1: //스턴
                    {
                        whostun.GetComponentInChildren<Animator>().SetBool("stun", true);
                        //whostun.GetComponent<FirstPersonController>().enabled = false;
                        break;
                    }
                case 2: //스턴 풀기
                    {
                        whostun.GetComponentInChildren<Animator>().SetBool("stun", false);
                        //whostun.GetComponent<FirstPersonController>().enabled = true;
                        break;
                    }
                case 3: //밀치기 애니메이션 동기화
                    {
                        whostun.GetComponentInChildren<Animator>().SetBool("Push", false);
                        break;
                    }
            }
        }
        //[PunRPC]
        //void AddDictionary(int id, int actorID)
        //{
        //    GameObject doll = PhotonView.Find(id).gameObject;
        //    Exit_GameManager.instance.players.Add(actorID, doll);
        //    dic = true;
        //}
        IEnumerator PushAnimation()
        {
            yield return new WaitForSeconds(0.2f);
            pv.RPC("DollCatchAnimationSycn", PhotonTargets.All, GetComponent<PhotonView>().viewID, 3);
        }
    }
}