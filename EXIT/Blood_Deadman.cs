﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blood_Deadman : Photon.PunBehaviour
{

    bool useSkill_Blood;    //블러드 시전여부
    [HideInInspector] public float coolTime_Blood = 20f;     //블러드 쿨타임
    [HideInInspector] public float leftTime_Blood;  //블러드 남은시간
    public AudioClip useBlindSound;
    void Awake()
    {
        leftTime_Blood = coolTime_Blood;
    }
    private void Start()
    {
        if (photonView.isMine) //사신 출구 진입 막기용
        {
            GameObject.Find("Exit").GetComponentInChildren<ExitHoll>().reapercollision.isTrigger = false;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (photonView.isMine)
        {
            if (Input.GetMouseButtonDown(1) && !useSkill_Blood)
            {
                Debug.Log("Use Skill Blood By DeadMan");
                //photonView.RPC("StartBloody", PhotonTargets.All);         //전체 플레이어에게
                //this.photonView.RPC("StartBloody", PhotonTargets.Others);    //본인을 제외한 나머지 플레이어에게
                photonView.RPC("FindChil", PhotonTargets.Others, 1, 0);
                useSkill_Blood = true;
                this.gameObject.GetComponent<PlayerManager>().useSkillSound.clip = useBlindSound;
                this.gameObject.GetComponent<PlayerManager>().useSkillSound.volume = 1f * Exit_DBAPI.instance.data.setSFX;
                this.gameObject.GetComponent<PlayerManager>().useSkillSound.Play();
            }

            if (useSkill_Blood)
            {
                leftTime_Blood = leftTime_Blood - Time.fixedDeltaTime;
                if (leftTime_Blood < 0)
                {
                    useSkill_Blood = false;
                    leftTime_Blood = coolTime_Blood;
                }
            }
        }
    }
}
