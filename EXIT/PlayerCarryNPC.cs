﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Com.MyCompany.MyGame
{
    [RequireComponent(typeof(PhotonView))]
    public class PlayerCarryNPC : Photon.MonoBehaviour{    
      
        public static GameObject localPlayerInstance;
        bool IsFiring;
        public GameObject carryPoint;
        GameObject carryObject;
        bool canCarryOn; //접촉했는지
        bool carryNow; //들고있는지
        bool isPickUp; //중력작용체크
        Rigidbody itemRigidbody;

    private void Awake()
    {
            //if (photonView.isMine)
            //{
            //    PlayerManager.localPlayerInstance = this.gameObject;
            //}
            //DontDestroyOnLoad(this.gameObject);
        }        
        void OnTriggerEnter(Collider other) //접촉 체크
        {
            if (other.CompareTag("NPC")&&!canCarryOn)//키네마틱 작용되고있다면 누군가 들고있음.
            {
                carryObject = other.gameObject;
                canCarryOn = true;
               if(carryObject.GetComponent<Rigidbody>().isKinematic == true)
                {
                    carryObject = null;
                    print("들고있음");
                    return;
                }
            }
            else
            {
                print("이미 들고있음");
                return;
            }
       
        }
       
        private void OnTriggerExit(Collider other) //접촉 벗어남 체크.
        {
            if (other.CompareTag("NPC")&&!carryNow)
            {
                print("들것없음");
                canCarryOn = false;
                carryObject = null;
            }
            else
            {
                
                return;
            }
        }
        // Update is called once per frame
        void Update () {
            //print("들것 : " + carryObject);
            //print("들수있냐" + canCarryOn);
            ////if (photonView.isMine)
            //{
            //  return;
                
            //}
               
            
            if (Input.GetButton("Fire1")&& canCarryOn) //잡기
            {
                if(carryObject.GetPhotonView().ownerId != PhotonNetwork.player.ID) //오브젝트 소유권이 없다면 소유권을 가져옴.
                {
                    print("소유권양도");
                    carryObject.GetPhotonView().RequestOwnership();
                    carryNow = true;
                    
                }
                else if(carryObject.GetPhotonView().ownerId == PhotonNetwork.player.ID) //이미 소유권이있다면
                {
                    carryNow = true;
                }
                print("들어올림");
               
            }
            if (carryObject != null)
            {

            if (carryObject.GetPhotonView().ownerId == PhotonNetwork.player.ID && carryNow)
            {
                print("들고있음");
                carryObject.transform.position = carryPoint.transform.position;
                carryObject.transform.rotation = carryPoint.transform.rotation;
                //carryObject.transform.parent = carryPoint.transform;
                if (!isPickUp)
                {
                    PickUp(carryObject); //중력작용

                }
                if (Input.GetKeyDown(KeyCode.R) && isPickUp)
                {
                    print("버림");
                    Drop(carryObject);

                }
            }
            
            }
        }

        private void PickUp(GameObject carryObject)
        {
            isPickUp = true; //키네마틱 True
            SetCarry(carryObject, true);
            photonView.RPC("RpcSetCarry", PhotonTargets.Others, carryObject.name, true);
        }
        
        void Drop(GameObject carriedOb)
        {          
            isPickUp = false; //키네마틱 false
            carryNow = false; //떨어짐.
            SetCarry(carriedOb, false); //자신의 키네마틱
            photonView.RPC("RpcSetCarry", PhotonTargets.Others, carriedOb.name, false);//동기화 키네마틱
            //arryPoint.transform.DetachChildren();
            canCarryOn = false;
        }
        void Still(GameObject getOb)
        {
            carryNow = false;//들고있음 false            
            canCarryOn = false;
            if (isPickUp)
            {
                photonView.RPC("RpcStill", PhotonTargets.Others, getOb.name);
            }
        }
        [PunRPC]
        void RpcStill(string carriedOb)
        {
            print("스틸당함");
            //SetCarry(carriedOb, true);
            GameObject stillObject = GameObject.Find(carriedOb).gameObject;
            if(stillObject.GetPhotonView().ownerId == PhotonNetwork.player.ID)
            {
                carryNow = true;
                isPickUp = false;
            }
            else
            {
                carryNow = false;
            }
        }
        private void SetCarry(GameObject carryObject, bool isCarry)
        {
            Collider[] itemColliders = carryObject.GetComponents<Collider>();
             itemRigidbody = carryObject.GetComponent<Rigidbody>();

            foreach (Collider itemCollider in itemColliders)
            {
                itemCollider.enabled = !isCarry;
            }
            itemRigidbody.isKinematic = isCarry;
            itemRigidbody.useGravity = !isCarry;
        }
        
        [PunRPC]
        void RpcSetCarry(string carryObject, bool isCarry) //플레이어가 잡은 오브젝트 중력효과 동기화.
        {
            print("동기화");
            GameObject rpcObject = GameObject.Find(carryObject).gameObject;
            Collider[] itemColliders = rpcObject.GetComponents<Collider>();
            itemRigidbody = rpcObject.GetComponent<Rigidbody>();

            foreach (Collider itemCollider in itemColliders)
            {
                itemCollider.enabled = !isCarry;
            }
            itemRigidbody.isKinematic = isCarry;
            itemRigidbody.useGravity = !isCarry;
        }


        private void OnGUI()
        {
            if(itemRigidbody != null)
                GUILayout.Label(itemRigidbody.isKinematic.ToString());
        }             
    }
    
}

