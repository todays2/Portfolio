﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ElixirViewer : MonoBehaviour {
    public RectTransform elixirViewer;
    public Text elixirCounter;
	
	// Update is called once per frame
	void Update () {
        elixirViewer.sizeDelta = new Vector2(MyGameManager.ins.blueElixir * 57.3f,elixirViewer.rect.height);
        elixirCounter.text = ((int)MyGameManager.ins.blueElixir).ToString();
	}
}
