﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingAI : MonoBehaviour {
    public Transform insPos;
    UnitState mystate;
    MyGameManager gm;
    UnitEffect myEffect;
    AudioSource myAudio;
    public GameObject healthPrefab;
    Image healthfilled;
    float damaged;
    bool isBlue = false;
	// Use this for initialization
	void Start () {
        if(tag == "BlueTeam")
        {
            isBlue = true;
        }
        mystate = GetComponent<UnitState>();
        myEffect = GetComponent<UnitEffect>();
        myAudio = GetComponent<AudioSource>();
        gm = MyGameManager.ins;
        damaged = mystate.maxHealth / mystate.power;
        SpawnSound();
        StartCoroutine(StartDamaged());
        StartCoroutine(Spawn());
        StartCoroutine(HealthBar());

    }
    
    IEnumerator HealthBar()
    {
        foreach (Canvas a in FindObjectsOfType<Canvas>())
        {
            if (a.renderMode == RenderMode.WorldSpace)
            {
                healthPrefab = Instantiate(healthPrefab, a.transform);
                healthfilled = healthPrefab.transform.GetChild(0).GetComponent<Image>();
            }
        }
        while (true)
        {
            if(mystate.health != mystate.maxHealth)
            {
                healthPrefab.SetActive(true);
                healthfilled.fillAmount = (float)mystate.health / (float)mystate.maxHealth;
                healthPrefab.transform.position = transform.GetChild(0).position;
            }
            if (mystate.health == 0)
            {
                healthPrefab.SetActive(false);
                gameObject.SetActive(false);
            }
            yield return null;
        }
    }

    IEnumerator StartDamaged()
    {
        while (true)
        {
            Damaged();
            yield return new WaitForSeconds(1);
        }
    }
    IEnumerator Spawn()
    {
        
        yield return new WaitForSeconds(1);
        while (true)
        {
            SpawnUnit();
            yield return new WaitForSeconds(mystate.spawnTime);
        }
    }
    void SpawnUnit()
    {
        switch (mystate.myName)
        {            
            case UnitState.MYNAME.BCAMP:
                {
                    if (isBlue)
                    {
                        for (int i = 0; i < gm.unitPrefabs.Count; i++)
                        {
                            if (UnitState.MYNAME.BABA == gm.unitPrefabs[i].GetComponent<UnitState>().myName)
                            {
                                Instantiate(gm.unitPrefabs[i], insPos.position, gm.unitPrefabs[i].transform.rotation);
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < gm.unitPrefabs.Count; i++)
                        {
                            if (UnitState.MYNAME.BABA == gm.unitPrefabs[i].GetComponent<UnitState>().myName)
                            {
                                Instantiate(gm.enemyPrefabs[i], insPos.position, gm.enemyPrefabs[i].transform.rotation);
                            }
                        }
                    }
                }
                break;
        }
       
    }
    void Damaged()
    {
        mystate.health = mystate.health < damaged ? 0 : mystate.health -=(int)damaged;       
    }
    void SpawnSound()
    {
        myAudio.clip = myEffect.spawnSound;
        myAudio.PlayOneShot(myAudio.clip);
    }
}
