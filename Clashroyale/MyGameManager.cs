﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyGameManager : MonoBehaviour {
    public static MyGameManager ins;
    public DropZone dropCard;
    public Transform build;
    public TowerAI blueKing;
    public UnitState blueKingTower;
    public TowerAI redKing;
    public UnitState redKingTower;
    public UnitState[] blueTowers;
    public UnitState[] redTowers;
    public MyTile myTile;
    public List<Transform> towers;
    public List<GameObject> unitPrefabs;
    public List<GameObject> enemyPrefabs;
    public List<GameObject> unitCards;
    public Transform nextCard;
    public Transform[] enemyInsPos;
    public GameObject hand;
    public GameObject gameOverPanel;
    public float blueElixir = 5;
    public float redElixir = 5;
    public bool gameOver;
    float nextCardCount = 3f;
    public int next = 4;
    public float nextCardCurCount = 0f;
    public int enemyCardIndex = 0;
    int e = 0;
    public AudioSource gmAudio;
    public UnitEffect gmSound;
    MyHand myhand;
    StartSceneManager st;

    private void Awake()
    {
        ins = this;
    }
    public void BuildSpriteChange(UnitState _BuildName)
    {
        for (int i = 0; i < unitPrefabs.Count; i++)
        {
            if (unitPrefabs[i].GetComponent<UnitState>().myName == _BuildName.myName)
            {
                if (unitPrefabs[i].GetComponent<UnitState>().myName == UnitState.MYNAME.ROKET)
                {
                    build.GetComponent<SpriteRenderer>().sprite = unitPrefabs[i].GetComponent<SpriteRenderer>().sprite;
                    Roket rk = unitPrefabs[i].GetComponent<Roket>();
                    build.transform.localScale = new Vector3(rk.range * .5f, rk.range * .5f, rk.range * .5f);
                }
                else
                {
                    build.GetComponent<SpriteRenderer>().sprite = unitPrefabs[i].GetComponentInChildren<SpriteRenderer>().sprite;
                    build.transform.localScale = new Vector3(1, 1, 1);
                }
            }
        }
    }
    // Use this for initialization
    void Start() {
        gmAudio = GetComponent<AudioSource>();
        gmSound = GetComponent<UnitEffect>();
        myhand = FindObjectOfType<MyHand>();
        st = FindObjectOfType<StartSceneManager>();
        for (int i = 0; i < myhand.selectedUnits.Count; i++)
        {
            int random = Random.Range(i, myhand.selectedUnits.Count);
            string tmp = myhand.selectedUnits[random];
            myhand.selectedUnits[random] = myhand.selectedUnits[i];
            myhand.selectedUnits[i] = tmp;
            //Debug.Log(random);
        }
        for (int i = 0; i < myhand.enemyUnits.Count; i++)
        {
            int random = Random.Range(i, myhand.enemyUnits.Count);
            string tmp = myhand.enemyUnits[random];
            myhand.enemyUnits[random] = myhand.enemyUnits[i];
            myhand.enemyUnits[i] = tmp;
            //Debug.Log(random);
        }
        StartCoroutine(HandCheck());
    }
    private void Update()
    {
        if (gameOver)
        {
            return;
        }
        GameOverCheck();
        ElixirUp();
        KingCheck();
        NextCardCount();
        EnemyCardUse();
    }
    void EnemyCardUse()
    {
        if(myhand.enemyUnits[enemyCardIndex] == UnitState.MYNAME.ROKET.ToString())
        {
            if (enemyCardIndex < 7)
            {
                enemyCardIndex++;
            }
            else
            {
                enemyCardIndex = 0;
            }
        }
        for (int i = 0; i < unitCards.Count; i++)
        {
            if(myhand.enemyUnits[enemyCardIndex] == unitCards[i].GetComponent<UnitState>().myName.ToString())
            {
                if(redElixir >= unitCards[i].GetComponent<UnitState>().elixir)
                {
                    redElixir -= unitCards[i].GetComponent<UnitState>().elixir;
                    if (unitCards[i].GetComponent<UnitState>().myType == UnitState.Type.BUILDING)
                    {
                        StartCoroutine(dropCard.DropCard(unitCards[i].GetComponent<UnitState>(), enemyInsPos[2].position, false));
                    }
                    else
                    {
                        StartCoroutine(dropCard.DropCard(unitCards[i].GetComponent<UnitState>(), enemyInsPos[e].position, false));
                        if(e == 0)
                        {
                            e = 1;
                        }
                        else
                        {
                            e = 0;
                        }
                    }
                    if (enemyCardIndex < 7)
                    {
                        enemyCardIndex++;
                    }
                    else
                    {
                        enemyCardIndex = 0;
                    }
                    //for (int j = 0; j < enemyPrefabs.Count; j++)
                    //{
                    //    if(MyHand.ins.enemyUnits[enemyCardIndex] == enemyPrefabs[i].GetComponent<UnitState>().myName.ToString())
                    //    {
                    //        if(enemyPrefabs[i].GetComponent<UnitState>().myType == UnitState.Type.BUILDING)
                    //        {
                    //            dropCard.DropCard(enemyPrefabs[i].GetComponent<UnitState>(), enemyInsPos[3].position, false);
                    //        }
                    //        else
                    //        {
                    //            int r = Random.Range(0, 1);
                    //            dropCard.DropCard(enemyPrefabs[i].GetComponent<UnitState>(), enemyInsPos[r].position, false);
                    //        }
                    //        if(enemyCardIndex < 7)
                    //        {
                    //            enemyCardIndex++;
                    //        }
                    //        else
                    //        {
                    //            enemyCardIndex = 0;
                    //        }
                    //    }
                    //}
                }
            }            
        }
    }
    void NextCardCount()
    {
        if(nextCardCurCount >= 0)
        {
            nextCardCurCount -= Time.deltaTime;
        }
    }
    void GameOverCheck()
    {
        if(blueKingTower.health <= 0)
        {
            gameOver = true;
            ShowResult(1);
        }
        if (redKingTower.health <= 0)
        {
            gameOver = true;
            ShowResult(0);
        }
    }
    void ElixirUp()
    {
        if (blueElixir < 10)
        {
            blueElixir += Time.deltaTime * 0.4f;
        }
        else
        {
            blueElixir = 10;
        }
        if (redElixir < 10)
        {
            redElixir += Time.deltaTime * 0.4f;
        }
        else
        {
            redElixir = 10;
        }
    }
    void KingCheck()
    {
        if (blueKingTower.health != blueKingTower.maxHealth && !blueKing.kingSuprised) //킹타워가 맞았을때
        {
            blueKing.KingLaunch();
        }
        for (int i = 0; i < blueTowers.Length; i++) //두 타워중 하나가 파괴 되면
        {
            //Debug.Log(blueTowers[i].health);
            if (blueTowers[i].health == 0 && !blueKing.kingSuprised)
            {
                blueKing.KingLaunch();
            }
        }
        if (redKingTower.health != redKingTower.maxHealth && !redKing.kingSuprised)
        {
            redKing.KingLaunch();
        }
        for (int i = 0; i < blueTowers.Length; i++)
        {
            if (redTowers[i].health == 0 && !redKing.kingSuprised)
            {
                redKing.KingLaunch();
            }
        }        
    }
    void ShowResult(int i)
    {
        gameOverPanel.SetActive(true);
        switch (i)
        {
            case 0: //Victory
                {
                    int r = Random.Range(0, gmSound.attackSounds.Length);
                    gmAudio.clip = gmSound.attackSounds[r];
                    gmAudio.PlayOneShot(gmAudio.clip);
                    gameOverPanel.transform.GetChild(0).gameObject.SetActive(true);
                }
                break;
            case 1: //Lose
                {
                    int r = Random.Range(0, gmSound.towerGoneSounds.Length);
                    gmAudio.clip = gmSound.towerGoneSounds[r];
                    gmAudio.PlayOneShot(gmAudio.clip);
                    gameOverPanel.transform.GetChild(1).gameObject.SetActive(true);
                }
                break;
        }
    }
    public void GameOverButton()
    {
        st.GameOver();
    }

    IEnumerator HandCheck() //나의 덱 순환
    {
        List<string> handList = myhand.selectedUnits;
        if(hand.transform.childCount <= 0) //카드 랜덤으로 첫 생성
        {
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < unitCards.Count; j++)
                {
                    if(handList[i] == unitCards[j].GetComponent<UnitState>().myName.ToString())
                    {
                        Instantiate(unitCards[j], hand.transform);
                    }
                }
            }
            hand.transform.GetChild(next).SetParent(nextCard);
            next++;
        }
        while (true)
        {
            yield return new WaitForSeconds(.1f);
            if(hand.transform.childCount <= 3)
            {
                if(nextCardCurCount <= 0) //카운트가 0이 되었다면
                {
                    nextCard.GetChild(0).SetParent(hand.transform); //핸드로 돌림
                    for (int i = 0; i < hand.transform.childCount; i++)
                    {
                        if(handList[next] == hand.transform.GetChild(i).GetComponent<UnitState>().myName.ToString()) //현재 돌아야 할 카드가 지금 가지고있는 카드중에 있다면 다음카드로 넘어간다.
                        {
                            next++;
                        }                       
                    }                    
                    for (int i = 0; i < unitCards.Count; i++) //카드를 생성한다.
                    {
                        if(handList[next] == unitCards[i].GetComponent<UnitState>().myName.ToString())
                        {
                            Instantiate(unitCards[i], nextCard); //다음 생성될 카드를 보여준다.
                        }
                    }
                    if(next < 7)
                    {
                        next++;
                    }
                    else
                    {
                        next = 0;
                    }
                    nextCardCurCount = nextCardCount;                    
                }
            }
            
            yield return null;
        }
    }
    public Transform TargetSet(Vector3 _UnitPos, bool _isBlue)
    {
        //Debug.Log(_UnitPos);
        //Debug.Log(towers[0].position.z);
        //Debug.Log(Mathf.Abs(_UnitPos.z - towers[0].position.z));
        if (_isBlue)
        {            
            if (_UnitPos.x <= 0)
            {
                //Debug.Log("작다");
                if (Mathf.Abs(_UnitPos.z - towers[0].position.z) > 0.4f)
                {
                    return towers[0];
                }
                else
                {                    
                    return towers[2];
                }                
            }
            else
            {
                //Debug.Log("크다");
                if (Mathf.Abs(_UnitPos.z - towers[1].position.z) > 0.4f)
                {
                    return towers[1];
                }
                else
                {
                    return towers[2];
                }
            }
        }
        else
        {
            if (_UnitPos.x <= 0)
            {
                if (Mathf.Abs(_UnitPos.z - towers[3].position.z) > 0.4f)
                {
                    return towers[3];
                }
                else
                {
                    return towers[5];
                }                
            }
            else
            {
                if (Mathf.Abs(_UnitPos.z - towers[4].position.z) > 0.4f)
                {
                    return towers[4];
                }
                else
                {
                    return towers[5];
                }                
            }         
        }
    }
	
}
