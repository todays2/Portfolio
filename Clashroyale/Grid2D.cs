﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid2D : MonoBehaviour
{

    public bool editMod;

    public float width = 32f;
    public float height = 32f;

    public Color color = Color.green;

    

    private void OnDrawGizmos()
    {
        if (editMod)
        {
            if (height > 0 && width > 0)
            {
                Vector3 pos = Camera.current.transform.position; //현재 카메라의 포지션
                Gizmos.color = color; //그리드 색 지정
                for (float y = pos.y - 800f; y < pos.y + 800f; y += height) // ex)높이 =3이면 0에 라인, 3에 라인, 6에 라인
                { //가로 라인 그리기
                    Gizmos.DrawLine(new Vector3(-1000000f, 0f, Mathf.Floor(y / height) * height), new Vector3(1000000f, 0f, Mathf.Floor(y / height) * height));
                    //시작포지션 ->Z를 높이에 맞게 X1~X2까지 선을 긋는다.     
                }
                for (float x = pos.x - 1200f; x < pos.x + 1200; x += width)
                {//세로 라인 그리기
                    Gizmos.DrawLine(new Vector3(Mathf.Floor(x / width) * width, 0f, -1000000f), new Vector3(Mathf.Floor(x / width) * height, 0f, 1000000f));
                }
            }
            else
            {
                Debug.Log("Error!!!! Your MeshRendere is Zero");
            }
        }
    }
}
