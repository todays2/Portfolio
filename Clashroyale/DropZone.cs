﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropZone : MonoBehaviour, IDropHandler, IPointerEnterHandler/*, IPointerExitHandler*/ {

    Vector3 insPos;
    public float insOffset;    

    public void OnDrop(PointerEventData eventData)
    {
        //Debug.Log(eventData.pointerDrag.name +"Was Dropped on" + gameObject.name);
        if(gameObject.tag == "DropArea" && MyGameManager.ins.myTile.canBuild)
        {
            UnitState card = eventData.pointerDrag.transform.GetComponent<UnitState>();            
            StartCoroutine(DropCard(card, MyGameManager.ins.myTile.targetPos, true));
            MyGameManager.ins.blueElixir -= card.elixir;
            eventData.pointerDrag.transform.parent = transform;
            string drop = eventData.pointerDrag.transform.GetComponent<UnitState>().myName.ToString();
            int index = 0;
            //for (int i = 0; i < MyHand.ins.selectedUnits.Count; i++)
            //{
            //    if(drop == MyHand.ins.selectedUnits[i])
            //    {
            //        index = i;
            //    }
            //}
            //MyHand.ins.selectedUnits[index] = MyHand.ins.selectedUnits[4];
            //MyHand.ins.selectedUnits[4] = drop;
            //for (int i = 4; i < MyHand.ins.selectedUnits.Count; i++)
            //{
            //    int random = Random.Range(i, MyHand.ins.selectedUnits.Count);
            //    string tmp = MyHand.ins.selectedUnits[random];
            //    MyHand.ins.selectedUnits[random] = MyHand.ins.selectedUnits[i];
            //    MyHand.ins.selectedUnits[i] = tmp;
            //    //Debug.Log(random);
            //}                                   
        }
        else
        {
            eventData.pointerDrag.transform.position = eventData.pointerDrag.transform.GetComponent<Dragable>().returnToPosition;
            MyGameManager.ins.build.position = new Vector3(100, 0.1f, 100);
            eventData.pointerDrag.transform.GetComponent<CanvasGroup>().alpha = 1;
        }
               
    }
    public IEnumerator DropCard(UnitState _Card , Vector3 _insPos, bool _isBlue)
    {
        MyGameManager unit = MyGameManager.ins;
        if (_isBlue)
        {
            int index = 0;
            for (int i = 0; i < unit.unitPrefabs.Count; i++)
            {
                //Debug.Log(_Card.myName);
                //Debug.Log(unit.unitPrefabs[i].GetComponent<UnitState>().myName);
                if (_Card.myName == unit.unitPrefabs[i].GetComponent<UnitState>().myName)
                {
                    index = i;
                }
            }
            yield return new WaitForSeconds(unit.unitPrefabs[index].GetComponent<UnitState>().insTime);
            if (_Card.myType == UnitState.Type.SPELL)
            {
                switch (_Card.myName)
                {
                    case UnitState.MYNAME.ROKET:
                        {
                            GameObject ins = Instantiate(unit.unitPrefabs[index], MyGameManager.ins.blueKing.transform.position, Quaternion.identity);
                            ins.GetComponent<Roket>().target = _insPos;
                        }
                        break;
                }
            }
            else
            {
                switch (_Card.myName)
                {
                    case UnitState.MYNAME.BABA:
                        {
                            Vector3[] babains = new Vector3[4];
                            babains[0] = new Vector3(_insPos.x + insOffset, _insPos.y, _insPos.z + insOffset);
                            babains[1] = new Vector3(_insPos.x - insOffset, _insPos.y, _insPos.z + insOffset);
                            babains[2] = new Vector3(_insPos.x + insOffset, _insPos.y, _insPos.z);
                            babains[3] = new Vector3(_insPos.x - insOffset, _insPos.y, _insPos.z);
                            for (int i = 0; i < 4; i++)
                            {
                                GameObject ins = Instantiate(unit.unitPrefabs[index]);
                                ins.transform.position = babains[i];
                            }
                        }
                        break;
                    case UnitState.MYNAME.ARCHER:
                        {
                            Vector3[] archerins = new Vector3[2];
                            archerins[0] = new Vector3(_insPos.x + insOffset, _insPos.y, _insPos.z);
                            archerins[1] = new Vector3(_insPos.x - insOffset, _insPos.y, _insPos.z);
                            for (int i = 0; i < 2; i++)
                            {
                                GameObject ins = Instantiate(unit.unitPrefabs[index]);
                                ins.transform.position = archerins[i];
                            }
                        }
                        break;
                    case UnitState.MYNAME.GOBLINE:
                        {
                            Vector3[] goblineins = new Vector3[3];
                            goblineins[0] = new Vector3(_insPos.x, _insPos.y, _insPos.z + insOffset);
                            goblineins[1] = new Vector3(_insPos.x + insOffset, _insPos.y, _insPos.z);
                            goblineins[2] = new Vector3(_insPos.x - insOffset, _insPos.y, _insPos.z);
                            for (int i = 0; i < 3; i++)
                            {
                                GameObject ins = Instantiate(unit.unitPrefabs[index]);
                                ins.transform.position = goblineins[i];
                            }
                        }
                        break;
                    default:
                        {
                            GameObject ins = Instantiate(unit.unitPrefabs[index]);
                            ins.transform.position = _insPos;
                        }
                        break;
                }
            }
            MyGameManager.ins.build.position = new Vector3(100, 0.1f, 100);
            //DestroyImmediate(eventData.pointerDrag.gameObject);
        }
        else
        {
            //Debug.Log("적");
            int index = 0;
            for (int i = 0; i < unit.enemyPrefabs.Count; i++)
            {
                //Debug.Log(_Card.myName);
                //Debug.Log(unit.enemyPrefabs[i].GetComponent<UnitState>().myName);
                if (_Card.myName == unit.enemyPrefabs[i].GetComponent<UnitState>().myName)
                {
                    index = i;
                }
            }
            yield return new WaitForSeconds(unit.enemyPrefabs[index].GetComponent<UnitState>().insTime);
            if (_Card.myType == UnitState.Type.SPELL)
            {
                switch (_Card.myName)
                {
                    case UnitState.MYNAME.ROKET:
                        {
                            GameObject ins = Instantiate(unit.enemyPrefabs[index], MyGameManager.ins.blueKing.transform.position, Quaternion.identity);
                            ins.GetComponent<Roket>().target = _insPos;
                        }
                        break;
                }
            }
            else
            {
                switch (_Card.myName)
                {
                    case UnitState.MYNAME.BABA:
                        {
                            Vector3[] babains = new Vector3[4];
                            babains[0] = new Vector3(_insPos.x + insOffset, _insPos.y, _insPos.z + insOffset);
                            babains[1] = new Vector3(_insPos.x - insOffset, _insPos.y, _insPos.z + insOffset);
                            babains[2] = new Vector3(_insPos.x + insOffset, _insPos.y, _insPos.z);
                            babains[3] = new Vector3(_insPos.x - insOffset, _insPos.y, _insPos.z);
                            for (int i = 0; i < 4; i++)
                            {
                                GameObject ins = Instantiate(unit.enemyPrefabs[index]);
                                ins.transform.position = babains[i];
                            }
                        }
                        break;
                    case UnitState.MYNAME.ARCHER:
                        {
                            Vector3[] archerins = new Vector3[2];
                            archerins[0] = new Vector3(_insPos.x + insOffset, _insPos.y, _insPos.z);
                            archerins[1] = new Vector3(_insPos.x - insOffset, _insPos.y, _insPos.z);
                            for (int i = 0; i < 2; i++)
                            {
                                GameObject ins = Instantiate(unit.enemyPrefabs[index]);
                                ins.transform.position = archerins[i];
                            }
                        }
                        break;
                    case UnitState.MYNAME.GOBLINE:
                        {
                            Vector3[] goblineins = new Vector3[3];
                            goblineins[0] = new Vector3(_insPos.x, _insPos.y, _insPos.z + insOffset);
                            goblineins[1] = new Vector3(_insPos.x + insOffset, _insPos.y, _insPos.z);
                            goblineins[2] = new Vector3(_insPos.x - insOffset, _insPos.y, _insPos.z);
                            for (int i = 0; i < 3; i++)
                            {
                                GameObject ins = Instantiate(unit.enemyPrefabs[index]);
                                ins.transform.position = goblineins[i];
                            }
                        }
                        break;
                    default:
                        {
                            GameObject ins = Instantiate(unit.enemyPrefabs[index]);
                            ins.transform.position = _insPos;
                        }
                        break;
                }
            }
            MyGameManager.ins.build.position = new Vector3(100, 0.1f, 100);
            //DestroyImmediate(eventData.pointerDrag.gameObject);
        }

        yield return null;
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        if(eventData.pointerDrag != null)
        {
            if (gameObject.tag == "DropArea")
            {
                eventData.pointerDrag.transform.GetComponent<CanvasGroup>().alpha = 0;
                MyGameManager.ins.BuildSpriteChange(eventData.pointerDrag.transform.GetComponent<UnitState>());
            }
            else
            {
                eventData.pointerDrag.transform.GetComponent<CanvasGroup>().alpha = 1;
            }
            //Debug.Log(eventData.pointerDrag.name + "OnPointerEnter" + gameObject.name);
        }
    }
    public void OnPointerExit(PointerEventData eventData)
    {       
        eventData.pointerDrag.transform.GetComponent<CanvasGroup>().alpha = 1;             
        //Debug.Log("OnPointerExit");
    }
}
