﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitEffect : MonoBehaviour {

    public GameObject throwOb;
    public AudioClip[] attackSounds;
    public AudioClip spawnSound;
    public AudioClip[] towerGoneSounds;

}
