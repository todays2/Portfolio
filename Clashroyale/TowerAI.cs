﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerAI : MonoBehaviour
{
    MyGameManager gm;
    Vector3 myForward;
    Vector3 targetVector;
    Vector3 myin;
    Vector3 myPos;
    public Sight3D mySight;
    UnitState myState;
    UnitEffect myEffect;
    AudioSource myAudio;
    public bool isBlue = false;
    public List<Collider> enemies = new List<Collider>();
    float angletoTarget;
    SpriteRenderer mySprite;
    public Animator unitAnim;
    public Animator canonIdleAnim;
    public Animator canonAnim;
    public GameObject target;
    public bool kingSuprised;
    bool imKing = false;
    public Image healthBar;
    Image healthFilled;
    Text health;
    

    public void OpenCanon()
    {
        canonIdleAnim.SetBool("open", true);
        myAudio.clip = myEffect.spawnSound;
        myAudio.PlayOneShot(myAudio.clip);
        StartCoroutine(OpenCanonCoroutine());
        //Debug.Log(Time.time);
    }

    public IEnumerator OpenCanonCoroutine()
    {
        while (!canonIdleAnim.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.OpenBox"))
        {
            yield return null;
        }        
        yield return new WaitForSeconds(canonIdleAnim.GetCurrentAnimatorStateInfo(0).length-.4f);
        canonIdleAnim.gameObject.SetActive(false);
        canonAnim.gameObject.SetActive(true);
        yield return null;
    }
    // Use this for initialization
    void Start()
    {
        gm = MyGameManager.ins;
        health = healthBar.GetComponentInChildren<Text>();
        healthFilled = healthBar.transform.GetChild(0).GetComponent<Image>();
        mySprite = canonAnim.transform.GetComponent<SpriteRenderer>();          
        myState = GetComponentInParent<UnitState>();
        mySight = GetComponent<Sight3D>();
        mySight.sightViewDistance = myState.attackRange;
        myEffect = GetComponent<UnitEffect>();
        myAudio = GetComponent<AudioSource>();
        myPos = transform.position;
        if (tag == "BlueTeam")
        {
            isBlue = true;
        }
        if(myState.myName == UnitState.MYNAME.KING)
        {
            imKing = true;
        }      
        StartCoroutine(TowerAction());
    }
    public void KingLaunch()
    {
        if (!kingSuprised)
        {
            unitAnim.SetBool("Surprise", true);
            int random = Random.Range(0, myEffect.towerGoneSounds.Length - 1);
            myAudio.clip = myEffect.towerGoneSounds[random];
            myAudio.PlayOneShot(myAudio.clip);
            kingSuprised = true;            
        }
    }
    void HealthBar()
    {
        if(myState.health != myState.maxHealth)
        {
            healthBar.gameObject.SetActive(true);
            healthFilled.fillAmount = (float)myState.health / (float)myState.maxHealth;
            health.text = myState.health.ToString();
        }
    }
    IEnumerator TowerAction()
    {
        while (true)
        {
            HealthBar();
            if (myState.health == 0)
            {
                healthBar.gameObject.SetActive(false);
                transform.parent.gameObject.SetActive(false);
            }            
            if (gm.gameOver)
            {
                canonAnim.SetBool("Attack", false);
                break;
            }
            if (canonAnim.gameObject.activeInHierarchy)
            {
                if (target == null)
                {
                    DisTanceCheck();
                }
                else
                {
                    if(Vector3.Distance(myPos, target.transform.position) < myState.attackRange)
                    {
                        if(imKing)
                        Launch();

                        canonAnim.SetBool("Attack", true);
                    }
                    else
                    {
                        DisTanceCheck();

                        if(!imKing)
                        canonAnim.SetBool("Attack", false);
                    }                    
                    if(imKing)
                    yield return new WaitForSeconds(myState.attackSpeed);                    
                }
                if(target != null)
                {
                    myForward = new Vector3(myPos.x, myPos.y, myPos.z + 1) - myPos;
                    targetVector = target.transform.position - myPos;

                    myin = transform.InverseTransformPoint(target.transform.position);
                    angletoTarget = Vector3.Angle(myForward, targetVector);
                    //Debug.DrawRay(transform.position, myForward, Color.cyan);
                    //Debug.DrawRay(transform.position, targetVector, Color.blue);
                    //Debug.Log(angletoTarget);
                    canonAnim.SetFloat("Angle", angletoTarget);

                    if (myin.x > 0f)
                    {
                        //Debug.Log(angletoTarget);
                        //Debug.Log("오른쪽");
                        mySprite.flipX = false;
                    }
                    else
                    {
                        //Debug.Log(angletoTarget);
                        //Debug.Log("왼쪽");
                        mySprite.flipX = true;
                    }
                    if (target.GetComponent<UnitState>().health == 0)
                    {
                        //target.gameObject.SetActive(false);
                        target = null;
                        canonAnim.SetBool("Attack", false);
                    }
                }
            }            
                yield return null;
        }
    }
    void DisTanceCheck()
    {
        //Debug.Log("Check");
        enemies = mySight.FindVisibleTargets(isBlue);
        //Debug.Log(enemies.Count);
        if (enemies.Count > 0)
        {
            int index = 0;
            for (int i = 0; i < enemies.Count; i++)
            {
                if (Vector3.Distance(myPos, enemies[index].transform.position) > Vector3.Distance(myPos, enemies[i].transform.position))
                {
                    index = i;
                }
            }
            target = enemies[index].gameObject;
        }
    }
    public void Launch()
    {
        if (gm.gameOver)
        {
            return;
        }
        //GameObject launchOb = ;
        if (target != null)
        {
            Throwing shoot = Instantiate(myEffect.throwOb).GetComponent<Throwing>();
            shoot.transform.position = myPos;
            shoot.target = target.transform;
            shoot.power = myState.power;
            shoot.targetState = target.GetComponent<UnitState>();
            myAudio.clip = myEffect.attackSounds[0];
            myAudio.PlayOneShot(myAudio.clip);
        }             
    }
    public void AniMationDelay()
    {
        if (gm.gameOver)
        {
            return;
        }
        StartCoroutine(Delayed());
    }
    IEnumerator Delayed()
    {
        if (gm.gameOver)
        {
            yield return null;
        }
        canonAnim.StartPlayback();
        yield return new WaitForSeconds(myState.attackSpeed);
        canonAnim.StopPlayback();
    }
    
}
