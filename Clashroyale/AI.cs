﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class AI : MonoBehaviour {
    Vector3 myForward; // 앞방향
    Vector3 myin; //이미지 전환용 방향
    SpriteRenderer mySprite; //받아올 이미지
    SpriteRenderer shadowSprite; //그림자용 이미지    
    float angletoTarget;    //앞방향에서 타겟방향의 각도
    Vector3 targetVector; //타겟의 벡터
    public Animator myAnim; //애니메이션
    public Animator shadowAnim; //그림자 애니메이션    
    public GameObject healthPrefab; //체력바 프리팹
    Image healthfilled; //체력바 동기화
    public Transform healthPos;//체력바 위치    
    UnitState myState; //내 정보
    Sight3D mySight; //내 시야체크
    UnitEffect myEffect; //내 이펙트 정보
    AudioSource myAudio; //내 오디오
    Rigidbody myrigid; //내 물리
    NavMeshAgent myNav; //내 AI
    public bool isBlue = false; //팀 확인용
    public bool isShadow = false;    //그림자 확인용
    public List<Collider> enemyCheck = new List<Collider>(); //공격범위체크
    public List<Collider> sightCheck = new List<Collider>();    //시야범위 체크    

    const float minPathUpdateTime = .2f; //새로운 길 찾는 주기
    const float pathUpdateMoveThreshold = .1f;

    public bool attacking;
    bool sounEffect;
    public float atCount = 0;
    bool canAT;
    int avoidancePriority;
    int downavoidancePriority;
    public Transform target; //이동 할 타겟 
    public Transform targetOb; //공격할 타겟
    public float speed = 1; //A*용 이동속도
    public float turnSpeed = 3;
    public float turnDst = 0;
    public float stoppingDst = 0;
    public bool followingPath; //길따라 가는중

    Path path;
    Vector3[] myWaypoints;   
               
    public enum STATE {IDLE,MOVE,ATTACK,ENEMYCHECK}
    public STATE state;
    void Start () {
        if (!isShadow)
        {
            myNav = GetComponentInParent<NavMeshAgent>();
            myrigid = GetComponentInParent<Rigidbody>();
            mySprite = GetComponent<SpriteRenderer>();
            shadowSprite = shadowAnim.transform.GetComponent<SpriteRenderer>();
            myState = GetComponentInParent<UnitState>();
            myrigid.mass = myState.weight;
            mySight = GetComponent<Sight3D>();
            myEffect = GetComponent<UnitEffect>();
            myAudio = GetComponent<AudioSource>();
            mySight.attackAreaViewDistance = myState.attackRange;
            //speed = myState.speed;        
            TeamSet();
            target = MyGameManager.ins.TargetSet(transform.position, isBlue);
            myNav.speed = myState.speed;
            avoidancePriority = myNav.avoidancePriority;
            downavoidancePriority = avoidancePriority - 1;
            SpwanSound();
            StartCoroutine(HealthBar());
            StartCoroutine(UpdatePath());            
            StartCoroutine(ShadowAnimation());
        }        
    }
    public void SoundEffect()
    {
        if (isShadow || sounEffect)
        {
            return;
        }
        sounEffect = true;
        int i = Random.Range(0, myEffect.attackSounds.Length - 1);
        myAudio.clip = myEffect.attackSounds[i];
        myAudio.PlayOneShot(myAudio.clip);
    }
    public void SpwanSound()
    {
        if (isShadow || myEffect.spawnSound == null)
        {
            return;
        }
        myAudio.clip = myEffect.spawnSound;
        myAudio.PlayOneShot(myAudio.clip);
    }
    void TeamSet()
    {
        if (tag == "BlueTeam")
        {
            isBlue = true;
        }
        else
        {
            isBlue = false;
        }
    }
    void LookAtTarget(Vector3 _Way)
    {
        myForward = new Vector3(transform.position.x, transform.position.y, transform.position.z + 1) - transform.position;
        targetVector = _Way - transform.position;

        myin = transform.InverseTransformPoint(_Way);
        angletoTarget = Vector3.Angle(myForward, targetVector);
        //Debug.DrawRay(transform.position, myForward * 10, Color.cyan);
        //Debug.DrawRay(transform.position, targetVector * 10, Color.blue);
        //Debug.Log(myin);
        if (atCount <= 0)
        {
            myAnim.SetFloat("Angle", angletoTarget);
        }           
        if (myin.x > 1f)
        {            
            mySprite.flipX = false;
            //shadowSprite.flipX = false;
        }
        else if(myin.x < 0f)
        {           
            mySprite.flipX = true;
            //shadowSprite.flipX = true;
        }
    }
    public void OnPathFound(Vector3[] waypoints, bool pathSuccessful)
    {
        if (pathSuccessful)
        {
            path = new Path(waypoints, transform.position, turnDst, stoppingDst);
            myWaypoints = waypoints;
            StopCoroutine("Action");
            StartCoroutine("Action");
        }
    }
    IEnumerator HealthBar()
    {
        foreach (Canvas a in FindObjectsOfType<Canvas>())
        {
            if (a.renderMode == RenderMode.WorldSpace)
            {
                healthPrefab = Instantiate(healthPrefab, a.transform);
                healthfilled = healthPrefab.transform.GetChild(0).GetComponent<Image>();
            }
        }
        while (true)
        {
            if (myState.health != myState.maxHealth)
            {
                healthPrefab.SetActive(true);
                healthfilled.fillAmount = (float)myState.health / (float)myState.maxHealth;
                healthPrefab.transform.position = healthPos.position;
            }
            yield return null;
        }
    }
    IEnumerator UpdatePath()
    {
        if (myState.myType == UnitState.Type.JUMP || myState.myType == UnitState.Type.AIR)
        {
            if (Time.timeSinceLevelLoad < .1f)
            {
                yield return new WaitForSeconds(.1f);
            }           
            PathRequestManager.noRequestPath(new PathRequest(transform.position, target.position, OnPathFound));

            float sqrMoveThreshold = pathUpdateMoveThreshold * pathUpdateMoveThreshold;
            Vector3 targetPosOld = target.position;

            while (true)
            {               
                if ((target.position - targetPosOld).sqrMagnitude > sqrMoveThreshold)
                {
                    PathRequestManager.RequestPath(new PathRequest(transform.position, target.position, OnPathFound));
                    targetPosOld = target.position;
                    //Debug.Log("NEWW");
                }                                
                yield return null;
            }
        }
        else
        {
            if (Time.timeSinceLevelLoad < .1f)
            {
                yield return new WaitForSeconds(.1f);
            }
            PathRequestManager.RequestPath(new PathRequest(transform.position, target.position, OnPathFound));

            float sqrMoveThreshold = pathUpdateMoveThreshold * pathUpdateMoveThreshold;
            Vector3 targetPosOld = target.position;

            while (true)
            {               
                if ((target.position - targetPosOld).sqrMagnitude > sqrMoveThreshold)
                {
                    PathRequestManager.RequestPath(new PathRequest(transform.position, target.position, OnPathFound));
                    targetPosOld = target.position;
                    //Debug.Log("NEWW");
                }               
                yield return null;
            }
        }
    }
    IEnumerator ShadowAnimation() //그림자 애니메이션 동기화
    {
        while (true)
        {
            shadowAnim.SetBool("Move", myAnim.GetBool("Move"));
            shadowAnim.SetBool("Attack", myAnim.GetBool("Attack"));
            shadowAnim.SetFloat("Angle", myAnim.GetFloat("Angle"));
            shadowSprite.flipX = mySprite.flipX;
            if (myState.myName == UnitState.MYNAME.HOGRIDER)
            {
                shadowAnim.SetBool("Jump", myAnim.GetBool("Jump"));
            }           
            yield return null;
        }
    }
    IEnumerator Action()
    {
        int pathIndex = 0;
        while (gameObject.activeInHierarchy)
        {
            if(myState.health <= 0)
            {
                healthPrefab.SetActive(false);
                transform.parent.gameObject.SetActive(false);
            }
            if (MyGameManager.ins.gameOver)
            {
                myAnim.SetBool("Move", false);
                myAnim.SetBool("Attack", false);
                break;
            }
            switch (state)
            {
                case STATE.IDLE:
                    {
                        myAnim.SetBool("Move", false);
                        myAnim.SetBool("Attack", false);
                        if (target != null)
                        {
                            state = STATE.MOVE;
                        }                        
                    }
                    break;
                case STATE.MOVE: //움직임
                    {
                        myNav.avoidancePriority = avoidancePriority;
                        LookAtTarget(myWaypoints[pathIndex]);
                        Vector2 pos2D = new Vector2(transform.position.x, transform.position.z);
                        while (path.turnBoundaries[pathIndex].HasCrossedLine(pos2D))
                        {
                            if (pathIndex == path.finishLineIndex)
                            {                                
                                state = STATE.IDLE;
                                myAnim.SetBool("Move", false);
                                //shadowAnim.SetBool("Move", false);                    
                                //attacking = true;
                                target = MyGameManager.ins.TargetSet(transform.position, isBlue);
                                break;
                            }
                            else
                            {
                                pathIndex++;
                            }
                        }
                        myAnim.SetBool("Move", true);
                        myAnim.SetBool("Attack", false);
                        myNav.isStopped = false;                            
                        myNav.SetDestination(myWaypoints[pathIndex]);
                        sightCheck = mySight.FindVisibleTargets(isBlue);
                        if (sightCheck.Count > 0)//시야 범위안에 들어온 적이 있나
                        {
                            EnemySelect(sightCheck); //시야범위에서 제일 가까운 놈을 적으로
                        }
                        else
                        {
                            target = MyGameManager.ins.TargetSet(transform.position, isBlue);
                        }
                    }
                    break;
                case STATE.ATTACK:
                    {                       
                        if(!attacking) //어택중이 아니면
                        {                            
                            LookAtTarget(targetOb.position);
                            myNav.SetDestination(myWaypoints[pathIndex]);
                            enemyCheck = mySight.FindAttackTarget(isBlue);
                            if (enemyCheck.Count > 0)
                            {
                                AttackCheck(enemyCheck);//공격범위안에 타겟이 들어왔다면                                
                            }
                            else
                            {
                                state = STATE.MOVE;
                            }
                        }
                        if(attacking)
                        {
                            myNav.avoidancePriority = downavoidancePriority;                                                        
                        }
                    }
                    break;
                case STATE.ENEMYCHECK:
                    {
                        Debug.Log("EnemyCheck");
                        myAnim.SetBool("Move", false);
                        myAnim.SetBool("Attack", false);
                        enemyCheck = mySight.FindAttackTarget(isBlue);                        
                        EnemySelect(enemyCheck);                                                   
                    }
                    break;
            }
            yield return null;
        }
    }
    void EnemyDeadCheck(UnitState _Enemy)
    {
        //Debug.Log(_Enemy);
        if (_Enemy.health <= 0)
        {            
            sounEffect = false;            
            myNav.isStopped = false;
            targetOb = null;            
            //target = MyGameManager.ins.TargetSet(transform.position, isBlue);
            attacking = false;
            state = STATE.ENEMYCHECK;
            //Debug.Log("EnemyDead");
        }
    }
    void AttackCheck(List<Collider> enemyList)
    {
        for (int i = 0; i < enemyList.Count; i++)
        {
            if (enemyList[i].transform == targetOb)
            {
                //shadowAnim.SetBool("Attack", true);
                attacking = true;
                myrigid.isKinematic = true;
                myrigid.isKinematic = false;
                myNav.isStopped = true;
                //StopCoroutine("FollowPath");
                myAnim.SetBool("Move", false);
                //shadowAnim.SetBool("Move", false);
                myAnim.SetBool("Attack", true);                
            }
        }
    }    
    public void AttackExit()
    {
        //Debug.Log("exit");
        if (isShadow)
        {
            return;
        }
        if (MyGameManager.ins.gameOver)
        {
            myAnim.SetBool("Move", false);
            myAnim.SetBool("Attack", false);
            return;
        }
        if (targetOb != null)
        {
            UnitState enemy = targetOb.GetComponent<UnitState>();
            switch (myState.myName)
            {
                case UnitState.MYNAME.BABA:
                    {
                        enemy.health = enemy.health < myState.power ? 0 : enemy.health - myState.power;
                        //EnemyDeadCheck(enemy);
                    }
                    break;
                case UnitState.MYNAME.ARCHER:
                    {
                        //GameObject arrow = 
                        Fire();
                    }
                    break;
                case UnitState.MYNAME.MUSKET:
                    {
                        //GameObject arrow = 
                        Fire();
                    }
                    break;
                case UnitState.MYNAME.GOBLINE:
                    {
                        enemy.health = enemy.health < myState.power ? 0 : enemy.health - myState.power;
                        //EnemyDeadCheck(enemy);
                    }
                    break;
                case UnitState.MYNAME.GIANT:
                    {
                        enemy.health = enemy.health < myState.power ? 0 : enemy.health - myState.power;
                        //EnemyDeadCheck(enemy);
                    }
                    break;
                case UnitState.MYNAME.BABYDRAGON:
                    {
                        Fire();
                        //EnemyDeadCheck(enemy);
                    }
                    break;
                case UnitState.MYNAME.HOGRIDER:
                    {
                        enemy.health = enemy.health < myState.power ? 0 : enemy.health - myState.power;
                        //EnemyDeadCheck(enemy);
                    }
                    break;
            }
            //StartCoroutine(WWWW());
        }
    }
    public void AniMationDelay()
    {
        StartCoroutine(Delayed());
    }
    IEnumerator Delayed()
    {
        if(this == null)
        {            
            yield return null;
        }
        if (!attacking)
        {
            yield return null;
        }        
        else
        {           
            myAnim.SetBool("Attack", false);
            myAnim.SetBool("Move", false);
            yield return new WaitForSeconds(myState.attackSpeed);
            if(targetOb != null)
            {
                LookAtTarget(targetOb.position);
            }
            sounEffect = false;
            myAnim.SetBool("Move", false);
            myAnim.SetBool("Attack", true);
        }
        if(targetOb != null)
        {
            EnemyDeadCheck(targetOb.GetComponent<UnitState>());
            DistanceCheck();
        }
    }
    void DistanceCheck()
    {
        enemyCheck = mySight.FindAttackTarget(isBlue);
        if(enemyCheck.Count > 0)
        {
            for (int i = 0; i < enemyCheck.Count; i++)
            {
                if (enemyCheck[i].transform == targetOb)
                {
                    return;
                }
            }
        }
        else
        {
            attacking = false;
            state = STATE.MOVE;
        }
    }
    void Fire()
    {
        Throwing shootArrow = Instantiate(myEffect.throwOb).GetComponent<Throwing>();
        shootArrow.transform.position = transform.position;
        shootArrow.target = targetOb;
        shootArrow.power = myState.power;
        shootArrow.targetState = targetOb.GetComponent<UnitState>();
        myAudio.clip = myEffect.attackSounds[0];
        myAudio.PlayOneShot(myAudio.clip);
    }
    void EnemySelect(List<Collider> enemyList)
    {
        int index = 0;
        List<Collider> filter = new List<Collider>();
        switch (myState.attackType)
        {
            case UnitState.AttackType.GROUND: //땅,건물
                {
                    for (int i = 0; i < enemyList.Count; i++)
                    {
                        if (enemyList[i].transform.GetComponent<UnitState>().myType != UnitState.Type.AIR)
                        {
                            filter.Add(enemyList[i]);
                        }
                    }
                    if (filter.Count > 0)
                    {
                        for (int i = 0; i < filter.Count; i++)
                        {
                            if (Vector3.Distance(transform.position, filter[index].transform.position) > Vector3.Distance(transform.position, filter[i].transform.position))
                            {
                                index = i;
                            }
                        }
                    }
                }
                break;
            case UnitState.AttackType.All://전부
                {
                    filter = enemyList;
                    if (filter.Count > 0)
                    {
                        for (int i = 0; i < filter.Count; i++)
                        {
                            if (Vector3.Distance(transform.position, filter[index].transform.position) > Vector3.Distance(transform.position, filter[i].transform.position))
                            {
                                index = i;
                            }
                        }
                    }
                }
                break;
            case UnitState.AttackType.ATB://빌딩만
                {
                    for (int i = 0; i < enemyList.Count; i++)
                    {
                        if (enemyList[i].transform.GetComponent<UnitState>().myType == UnitState.Type.BUILDING)
                        {
                            filter.Add(enemyList[i]);
                        }
                    }
                    if (filter.Count > 0)
                    {
                        for (int i = 0; i < filter.Count; i++)
                        {
                            if (Vector3.Distance(transform.position, filter[index].transform.position) > Vector3.Distance(transform.position, filter[i].transform.position))
                            {
                                index = i;
                            }
                        }
                    }
                }
                break;
        }
        if (filter.Count > 0)
        {
            if (filter[index].transform.GetComponent<UnitState>().myType == UnitState.Type.BUILDING)
            {
                target = filter[index].transform.GetChild(0).transform;
            }
            else
            {
                target = filter[index].transform;
            }
            targetOb = filter[index].transform;
            state = STATE.ATTACK;
        }
        else
        {
            Debug.Log("적이없다");
            target = MyGameManager.ins.TargetSet(transform.position, isBlue);
            targetOb = null;
            state = STATE.MOVE;
        }
    }    
}
