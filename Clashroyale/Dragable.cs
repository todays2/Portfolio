﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class Dragable : MonoBehaviour, IBeginDragHandler, IDragHandler,IEndDragHandler {
    public bool isInventory = false;
    public Vector3 returnToPosition;
    public bool canUse;
    MyGameManager gm;
    private void Start()
    {
        gm = MyGameManager.ins;
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        //Debug.Log("On Begin Drag");
        if (!isInventory)
        {
            returnToPosition = transform.position;
            GetComponent<CanvasGroup>().blocksRaycasts = false;
            gm.gmAudio.clip = gm.gmSound.spawnSound;
            gm.gmAudio.PlayOneShot(gm.gmAudio.clip);
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        //Debug.Log("On Drag");
        
        if (!isInventory)
        {
            if(MyTile.ins != null)
            {
                MyTile.ins.MouseRay(eventData.pointerDrag.GetComponent<UnitState>());
            }
            transform.position = eventData.position;
        }
        else
        {
            StartSceneManager.ins.ButtonHide();
        }
        
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("End Drag");
        if (!isInventory)
        {
            GetComponent<CanvasGroup>().blocksRaycasts = true;
        }
    }
}
