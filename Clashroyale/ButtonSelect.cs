﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSelect : MonoBehaviour {

    public Text elixir;
    StartSceneManager st;
    private void Start()
    {
        elixir.text = GetComponent<UnitState>().elixir.ToString();
        st = FindObjectOfType<StartSceneManager>();
    }
    public void ButtonPos()
    {        
        if(this.transform.parent.transform == st.inventory.transform)
        {
            st.handNow = false;
        }
        else
        {
            st.handNow = true;
        }
        st.useButtonPos = transform.gameObject;
    }
    public void Selected()
    {        
        st.select = GetComponent<UnitState>().myName.ToString();
        st.UnitSelect();
    }
}
