﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyTile : MonoBehaviour {
    public static MyTile ins;
    MyGameManager dd = MyGameManager.ins;
    Grid2D grid;
    GameObject target;
    Vector3 mousePos;
    public LayerMask buildMask;
    public Vector3 targetPos;
    public bool canBuild;
	// Use this for initialization
	void Start () {
        ins = this;
        grid = GetComponent<Grid2D>();
        target = MyGameManager.ins.build.gameObject;
	}
	
	// Update is called once per frame
	void Update () {
        //MouseRay();
       
    }

    public void MouseRay(UnitState _Card)
    {
        //RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector3.zero);
       
        
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray,out hit))
        {
            //Debug.Log(hit.transform.name);
            if(_Card.myType == UnitState.Type.SPELL)
            {
                canBuild = true;
                if (hit.transform && hit.transform.tag != "Wall")
                {
                    //Debug.Log(hit.transform.tag);
                    Vector3 mousePos = hit.point;
                    //Debug.Log(mousePos);
                    //Debug.Log((Mathf.Floor(mousePos.x / grid.width)));
                    Vector3 aligned = new Vector3((Mathf.Floor(mousePos.x / grid.width) * grid.width) + (grid.width / 2f), 0
                                                            , (Mathf.Floor(mousePos.z / grid.height) * grid.height) + (grid.height / 2f));
                    //Debug.Log(aligned);
                    target.transform.position = new Vector3(aligned.x, target.transform.position.y, aligned.z);
                    targetPos = target.transform.position;
                }
            }            
            else
            {
                canBuild = true;
                if (hit.transform && hit.transform.tag =="Arena")
                {
                    //Debug.Log(hit.transform.tag);
                    Vector3 mousePos = hit.point;
                    //Debug.Log(mousePos);
                    //Debug.Log((Mathf.Floor(mousePos.x / grid.width)));
                    Vector3 aligned = new Vector3((Mathf.Floor(mousePos.x / grid.width) * grid.width) + (grid.width / 2f), 0
                                                            , (Mathf.Floor(mousePos.z / grid.height) * grid.height) + (grid.height / 2f));
                    //Debug.Log(aligned);
                    target.transform.position = new Vector3(aligned.x, target.transform.position.y, aligned.z);
                    targetPos = target.transform.position;
                }
            }
            if (_Card.myType == UnitState.Type.BUILDING)
            {
                RaycastHit[] col = Physics.BoxCastAll(MyGameManager.ins.build.transform.position, new Vector3(0.6825f,0,0.555f), Vector3.up, Quaternion.identity,Mathf.Infinity, buildMask);
                if(col.Length > 0)
                {
                    MyGameManager.ins.build.GetComponent<SpriteRenderer>().color = Color.red;
                    canBuild = false;
                }
                else
                {
                    MyGameManager.ins.build.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 150);
                    canBuild = true;
                }       
            }

        }
        
            //Ray ray = Camera.current.ScreenPointToRay(new Vector3(mouse.x, -mouse.y + Camera.current.pixelHeight));
            //Vector3 aligned = new Vector3((Mathf.Floor(mousePos.x / grid.width) * grid.width) + grid.width / 2f,
            //                                            (Mathf.Floor(mousePos.y / grid.height) * grid.height) + grid.height / 2f, 0);

            //target.transform.position = aligned;
        //if (hit)
        //{
        //    //Debug.Log(hit.point);
        //}              
    }
    //private void OnDrawGizmos() //건물 칸 그림
    //{
    //    Gizmos.DrawWireCube(MyGameManager.ins.build.transform.position, new Vector3(0.6825f, 0, 0.555f));
    //}
}
