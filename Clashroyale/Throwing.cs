﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Throwing : MonoBehaviour {

    public Transform target;    
    public int power;
    public float speed;
    public UnitState targetState;
    public bool splash;
    public float splashRange = .3f;

    private void Start()
    {
        StartCoroutine(Fly());
    }
    //private void OnDrawGizmos()
    //{

    //    Gizmos.DrawSphere(transform.position, .3f);
    //}
    public IEnumerator Fly()
    {
        while (true)
        {
            if(target != null)
            {
                //Debug.Log("날아간다");
                Vector3 dir = target.position - transform.position;
                transform.LookAt(target);
                transform.Translate(Vector3.forward * speed * Time.deltaTime);
                if(Vector3.Distance(transform.position, target.position) < 0.2f)
                {
                    if (splash)
                    {
                        Collider[] splash = Physics.OverlapSphere(transform.position, splashRange);
                        List<Collider> filter = new List<Collider>();
                        for (int i = 0; i < splash.Length; i++)
                        {
                            if(splash[i].tag == target.tag)
                            {
                                filter.Add(splash[i]);
                            }
                        }
                        for (int i = 0; i < filter.Count; i++)
                        {
                            filter[i].GetComponent<UnitState>().health = filter[i].GetComponent<UnitState>().health < power ? 0 : targetState.health - power;
                        }
                        DestroyImmediate(this.gameObject);
                    }
                    else if(!splash)
                    {
                        targetState.health = targetState.health < power ? 0 : targetState.health - power;
                        DestroyImmediate(this.gameObject);
                    }
                }
            }
            yield return null;
        }       
    }    
}
