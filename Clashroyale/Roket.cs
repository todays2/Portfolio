﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Roket : MonoBehaviour {
    public Vector3 target;
    public Animator myanim;
    AudioSource myAudio;
    public GameObject exPlosion;
    public GameObject circle;
    Sight3D targets;
    public float speed;
    public float range;
    public int power;
    public int towerDamege;
    
	// Use this for initialization
	void Start () {
        //myanim = GetComponent<Animator>();
        myAudio = GetComponent<AudioSource>();        
        StartCoroutine(Fly());        
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.Log(myanim.GetFloat("Distance"));
	}

    public IEnumerator Fly()
    {
        circle = Instantiate(circle, target, Quaternion.identity);
        targets = circle.GetComponent<Sight3D>();
        targets.attackAreaViewDistance = range;
        circle.transform.localScale = new Vector3(range * .5f, range * .5f, range * .5f);
        while (true)
        {
            if (target != Vector3.zero)
            {
                //Debug.Log("날아간다");
                Vector3 dir = target - transform.position;
                transform.LookAt(target);
                transform.Translate(Vector3.forward * speed * Time.deltaTime);
                myanim.SetFloat("Distance",Vector3.Distance(transform.position, target));
                
                if (Vector3.Distance(transform.position, target) < 0.2f)
                {
                    myAudio.PlayOneShot(myAudio.clip);
                    myanim.gameObject.SetActive(false);
                    GameObject ex =  Instantiate(exPlosion, transform.position,Quaternion.identity,transform);

                    List<Collider> check = targets.FindAttackTarget(true);                    
                    for (int i = 0; i < check.Count; i++)
                    {
                        //Debug.Log(check[i].name);
                        if(check[i].GetComponent<UnitState>().myType == UnitState.Type.BUILDING)
                        {
                            check[i].GetComponent<UnitState>().health = check[i].GetComponent<UnitState>().health < towerDamege ? 0 : check[i].GetComponent<UnitState>().health - towerDamege;
                        }
                        else
                        {
                            check[i].GetComponent<UnitState>().health = check[i].GetComponent<UnitState>().health < power ? 0 : check[i].GetComponent<UnitState>().health - power;
                        }
                    }
                    DestroyImmediate(circle);
                    StartCoroutine(DesTroy());
                    break;
                   
                    //if (splash)
                    //{
                    //    Collider[] splash = Physics.OverlapSphere(transform.position, splashRange);
                    //    List<Collider> filter = new List<Collider>();
                    //    for (int i = 0; i < splash.Length; i++)
                    //    {
                    //        if (splash[i].tag == target.tag)
                    //        {
                    //            filter.Add(splash[i]);
                    //        }
                    //    }
                    //    for (int i = 0; i < filter.Count; i++)
                    //    {
                    //        filter[i].GetComponent<UnitState>().health = filter[i].GetComponent<UnitState>().health < power ? 0 : targetState.health - power;
                    //    }
                    //}
                    //else
                    //{
                    //    //targetState.health = targetState.health < power ? 0 : targetState.health - power;
                    //}
                    //DestroyImmediate(this.gameObject);
                }
            }
            yield return null;
        }
    }   
    IEnumerator DesTroy()
    {
        while (true)
        {
            if (!myAudio.isPlaying)
            {                
                DestroyImmediate(gameObject);
            }
            yield return null;
        }
    }
}
