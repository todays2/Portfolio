﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartSceneManager : MonoBehaviour {
    public static StartSceneManager ins;
    PluginManager pl;
    public GameObject st;
    public Image startImage;
    public GameObject myHand;
    public GameObject inventory;
    public GameObject swap;
    Color fadeOutColor;
    public Button useButton;
    public Button unUseButton;
    public GameObject useButtonPos;
    public bool handNow;
    AudioSource myAudio;
    public AudioClip[] clips;
    MyHand mh;
    
    public string select;
    private void Start()
    {
        if (ins != null)
        {
            Destroy(gameObject);
        }
        else
        {
            ins = this;
        }
        st = FindObjectOfType<Canvas>().gameObject;
        pl = FindObjectOfType<PluginManager>();
        DontDestroyOnLoad(gameObject);
        fadeOutColor = new Color(0,0,0,0);
        StartCoroutine(FadeOut(startImage));
        myAudio = GetComponent<AudioSource>();
        mh = FindObjectOfType<MyHand>();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pl.ExitPopUp();
        }
    }
    void SortMyHand()
    {
        if(myHand.transform.childCount > 1)
        {
            for (int i = 0; i < myHand.transform.childCount; i++)
            {
                for (int j = 0; j < myHand.transform.childCount-1; j++)
                {
                    if(myHand.transform.GetChild(j).GetComponent<UnitState>().elixir > myHand.transform.GetChild(j + 1).GetComponent<UnitState>().elixir)
                    {
                        GameObject tmp = myHand.transform.GetChild(j).gameObject;
                        myHand.transform.GetChild(j).SetParent(null);
                        tmp.transform.SetParent(myHand.transform);
                    }
                }
            }
            for (int i = 0, j = 1; i < myHand.transform.childCount; i++)
            {
                if(j < myHand.transform.childCount)
                {
                    if(myHand.transform.GetChild(i).GetComponent<UnitState>().elixir > myHand.transform.GetChild(j).GetComponent<UnitState>().elixir)
                    {
                        SortMyHand();
                    }
                    j++;
                }
            }
        }        
    }
    IEnumerator FadeOut(Image _image)
    {
        yield return new WaitForSeconds(1f);
        _image.CrossFadeColor(fadeOutColor, 1f, true, true);
        yield return new WaitForSeconds(1f);
        _image.gameObject.SetActive(false);
        yield return null;
    }
    public void UnitSelect()
    {
        //Debug.Log("dddd");        
        if(swap.transform.childCount == 0)
        {
            if (handNow)//핸드 소지중
            {
                //Debug.Log("소지중");
                unUseButton.gameObject.SetActive(true);
                useButton.gameObject.SetActive(false);
                unUseButton.transform.position = new Vector3(useButtonPos.transform.position.x, useButtonPos.transform.position.y - 50f, useButtonPos.transform.position.z);
            }
            else //핸드 소지 아님
            {
                //Debug.Log("소지중아님!");
                unUseButton.gameObject.SetActive(false);
                useButton.gameObject.SetActive(true);
                useButton.transform.position = new Vector3(useButtonPos.transform.position.x, useButtonPos.transform.position.y - 50f, useButtonPos.transform.position.z);
            }
        }
        else //스왑(8개 다찬경우)
        {
            if(useButtonPos.gameObject != swap.transform.GetChild(0).gameObject)
            Swap();
        }

    }
    public void UseUnit()
    {
        myAudio.clip = clips[4];
        myAudio.PlayOneShot(myAudio.clip);
        if (handNow)//핸드 소지중
        {
            mh.selectedUnits.Remove(select);
            mh.enemyUnits.Remove(select);
            useButtonPos.transform.SetParent(inventory.transform);
            unUseButton.gameObject.SetActive(false);
            myAudio.clip = clips[1];
            myAudio.PlayOneShot(myAudio.clip);
        }
        else //핸드 소지 아님
        {
            if (mh.selectedUnits.Count < 8)
            {
                mh.selectedUnits.Add(select);
                mh.enemyUnits.Add(select);
                useButtonPos.transform.SetParent(myHand.transform);
                useButton.gameObject.SetActive(false);
                if(mh.selectedUnits.Count == 8)
                {
                    SortMyHand();
                }
            }
            else
            {
                useButtonPos.gameObject.transform.SetParent(swap.transform);
                useButton.gameObject.SetActive(false);
                inventory.SetActive(false);                
            }
            myAudio.clip = clips[0];
            myAudio.PlayOneShot(myAudio.clip);
        }
    }
    public void Swap()
    {
        myAudio.clip = clips[2];
        myAudio.PlayOneShot(myAudio.clip);
        mh.selectedUnits.Remove(useButtonPos.GetComponent<UnitState>().myName.ToString());
        mh.selectedUnits.Add(swap.transform.GetChild(0).GetComponent<UnitState>().myName.ToString());
        mh.enemyUnits.Remove(useButtonPos.GetComponent<UnitState>().myName.ToString());
        mh.enemyUnits.Add(swap.transform.GetChild(0).GetComponent<UnitState>().myName.ToString());
        useButtonPos.gameObject.transform.parent = inventory.transform;
        swap.transform.GetChild(0).parent = myHand.transform;
        useButton.gameObject.SetActive(false);
        SortMyHand();
        inventory.SetActive(true);
    }
    public void ButtonHide()
    {
        useButton.gameObject.SetActive(false);
        unUseButton.gameObject.SetActive(false);
    }
    public void GameStart()
    {        
        if (mh.selectedUnits.Count == 8)
        {
            myAudio.clip = clips[3];
            myAudio.PlayOneShot(myAudio.clip);
            st.gameObject.SetActive(false);
            SceneManager.LoadScene("Arena");
        }        
    }
    public void GameOver()
    {
        st.gameObject.SetActive(true);
        SceneManager.LoadScene("StartsScenes");       
    }
}
