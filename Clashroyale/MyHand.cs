﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyHand : MonoBehaviour {

    public static MyHand ins;

    public List<string> selectedUnits = new List<string>(8);
    public List<string> enemyUnits = new List<string>(8);
    // Use this for initialization
    void Start () {
        if (ins != null)
        {
            Destroy(gameObject);
        }
        ins = this;
        DontDestroyOnLoad(gameObject);
    }	
}
