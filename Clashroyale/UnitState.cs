﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitState : MonoBehaviour {

    public enum MYNAME {BABA, ARCHER, GOBLINE, GIANT, KING, TOWER, BABYDRAGON, ROKET, HOGRIDER, BCAMP, MUSKET}
    public enum AttackType {GROUND, All, ATB}//땅만,공중만,전부,빌딩만
    public enum Type {GROUND,AIR,BUILDING,SPELL,JUMP}
    public MYNAME myName;
    public Type myType;
    public AttackType attackType;

    public int maxHealth;
    public int health;
    public int power;
    public float attackSpeed;
    public float speed;
    public float attackRange;
    public float insTime;
    public float spawnTime;
    public int elixir;
    public int weight;


	// Use this for initialization
	void Start () {
        
    }
        // Update is called once per frame
        void Update () {
		
	    }
}