﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitJump : MonoBehaviour {
  
    private void OnTriggerStay(Collider other)
    {        
        if (other.transform.GetComponent<UnitState>().myName == UnitState.MYNAME.HOGRIDER)
        {
            other.transform.GetComponentInChildren<AI>().myAnim.SetBool("Jump", true);
            other.transform.localScale = new Vector3(1.1f, 1.1f, 1.1f);
        }
    }
    private void OnTriggerExit(Collider other)
    {       
        if (other.transform.GetComponent<UnitState>().myName == UnitState.MYNAME.HOGRIDER)
        {
            other.transform.GetComponentInChildren<AI>().myAnim.SetBool("Jump", false);
            other.transform.localScale = new Vector3(1, 1, 1);
        }
    }
}
