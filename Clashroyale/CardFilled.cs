﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardFilled : MonoBehaviour {

    public Image filedImage;
    public Text elixirTxt;
    UnitState cardState;
    Dragable cardDrag;
    MyGameManager gm;

    private void Start()
    {
        gm = MyGameManager.ins;
        cardState = GetComponent<UnitState>();
        elixirTxt.text = cardState.elixir.ToString();
        cardDrag = GetComponent<Dragable>();
        StartCoroutine(CardCheck());
    }
    IEnumerator CardCheck()
    {
        while (true)
        {
            if(transform.parent == gm.nextCard)
            {
                if(gm.nextCardCurCount <= 0)
                {
                    filedImage.fillAmount = 1;
                }
                else
                {
                    filedImage.fillAmount = 0;
                }
                cardDrag.enabled = false;
            }
            else
            {
                filedImage.fillAmount = gm.blueElixir / cardState.elixir;
                if(filedImage.fillAmount == 1)
                {
                    cardDrag.enabled = true;
                }
                else
                {
                    cardDrag.enabled = false;
                }
            }

            yield return null;
        }
    }
}
